---
title: "refactor-cord"
author: "Louie Dinh"
date: "May 17, 2016"
output: html_document
---

```{r}
library(minfi)
library(tidyverse)

source("../lib/deconvolution-utils.R")
```

```{r}
# Load the preprocessed betas.
validation_betas <- read.table("../Data/validation_betas_filter_noob_combat.csv", sep=",") %>% as.matrix()
validation_betas_df <- as.data.frame(validation_betas)
validation_betas_df <- cbind(Probe=row.names(validation_betas_df), validation_betas_df)
row.names(validation_betas_df) <- NULL
# Write it out in a way refactor can understand
write.table(validation_betas_df, file="validation_betas.csv", row.names = F,
            col.names = T, sep="\t", quote=F)

validation_counts <- read.table("/Users/louie/Projects/cord-blood-deconvolution/Data/validation-counts.csv", header=T, sep=",")
row.names(validation_counts) <- validation_counts$SampleName
validation_counts$SampleName <- NULL
validation_counts <- data.matrix(validation_counts)
validation_counts <- validation_counts / 100
colnames(validation_counts) <- to_canonical_cell_types(colnames(validation_counts),
                        c("CD8T", "CD4T", "NK", "Bcell", "Monocytes", "Granulocytes", "nRBCs"),
                        c("CD8T","CD4T","NK","Bcell","Mono", "Gran", "nRBC"))
validation_counts <- cbind(SampleName=row.names(validation_counts), data.frame(validation_counts))
row.names(validation_counts) <- NULL
```

Calculate Refactor components
```{r, eval=F}
source("refactor/R/refactor.R")
k = 7
datafile = "validation_betas.csv"
results <- refactor(datafile,k)
components <- results$refactor_components # Extract the ReFACTor components
ranked_list <- results$ranked_list # Extract the list of sites ranked by ReFACTor

row.names(components) <- colnames(validation_betas)
ranked_probe_list <- row.names(validation_betas)[ranked_list]
```

```{r, eval=F}
sample2array <- read.csv("/Users/louie/Projects/cord-blood-deconvolution/Data/validation-sample_to_array.csv", header=T, stringsAsFactors = F)

# Map the array_ids to sample_ids
idx <- match(row.names(components), validation_counts$SampleName)
data <- cbind(components, validation_counts[idx,])

# Check regression against the cell counts
# Join the counts and the componenets
# This actually drops TS229, which we don't have validation data for.

# Map the array_ids to sample_ids
idx <- match(row.names(components), validation_counts$SampleName)
data <- cbind(validation_counts[idx,], components)

write.table(data, file="refactor_and_count_data_7_pcs.csv")
```

```{r}
data <- read.table("refactor_and_count_data_7_pcs.csv")
```

Plots
```{r}
data.tidy <- gather(data, CellType, CellProportion, -SampleName, -PC1, -PC2, -PC3, -PC4, -PC5, -PC6, -PC7)
ggplot(data.tidy, aes(PC1, CellProportion)) + geom_point() + facet_wrap(~ CellType, scales = "free")
ggplot(data.tidy, aes(PC2, CellProportion)) + geom_point() + facet_wrap(~ CellType, scales = "free")
ggplot(data.tidy, aes(PC3, CellProportion)) + geom_point() + facet_wrap(~ CellType, scales = "free")
ggplot(data.tidy, aes(PC4, CellProportion)) + geom_point() + facet_wrap(~ CellType, scales = "free")
ggplot(data.tidy, aes(PC5, CellProportion)) + geom_point() + facet_wrap(~ CellType, scales = "free")
ggplot(data.tidy, aes(PC6, CellProportion)) + geom_point() + facet_wrap(~ CellType, scales = "free")
ggplot(data.tidy, aes(PC7, CellProportion)) + geom_point() + facet_wrap(~ CellType, scales = "free")
```

See how much each PC can be explained by the cell type proportions.
```{r}
res <- NULL
fit <- lm("PC1 ~ Gran + Mono + CD4T + CD8T + Bcell + nRBC + NK", data=data)
res <- rbind(res, data.frame(PC=1, RsqCT=summary(fit)$r.squared))
fit <- lm("PC2 ~ Gran + Mono + CD4T + CD8T + Bcell + nRBC + NK", data=data)
res <- rbind(res, data.frame(PC=2, RsqCT=summary(fit)$r.squared))
fit <- lm("PC3 ~ Gran + Mono + CD4T + CD8T + Bcell + nRBC + NK", data=data)
res <- rbind(res, data.frame(PC=3, RsqCT=summary(fit)$r.squared))
fit <- lm("PC4 ~ Gran + Mono + CD4T + CD8T + Bcell + nRBC + NK", data=data)
res <- rbind(res, data.frame(PC=4, RsqCT=summary(fit)$r.squared))
fit <- lm("PC5 ~ Gran + Mono + CD4T + CD8T + Bcell + nRBC + NK", data=data)
res <- rbind(res, data.frame(PC=5, RsqCT=summary(fit)$r.squared))
fit <- lm("PC6 ~ Gran + Mono + CD4T + CD8T + Bcell + nRBC + NK", data=data)
res <- rbind(res, data.frame(PC=6, RsqCT=summary(fit)$r.squared))
fit <- lm("PC7 ~ Gran + Mono + CD4T + CD8T + Bcell + nRBC + NK", data=data)
res <- rbind(res, data.frame(PC=7, RsqCT=summary(fit)$r.squared))

res
```

Figure out how much variance is at each individual site.
```{r}
datafile = "validation_betas.csv"
t = 500
O = as.matrix(read.table(datafile))
sample_id <- O[1, -1] # extract samples ID
O <- O[-1,] # remove sample ID from matrix
cpgnames <- O[, 1] ## set rownames
O <- O[, -1] 
O = matrix(as.numeric(O),nrow=nrow(O),ncol=ncol(O))
```

```{r}
# Load in the ranked list of probes and take the top 500
ranked_list <- scan("refactor.out.rankedlist.txt", what="character")

# Scale our matrix.
print('Compute ReFACTor components...')
sites = match(ranked_list[1:t], cpgnames)
scaleO <- scale(t(O[sites,]))
pcs = prcomp(scaleO)
score = pcs$x
summary(pcs)

# Compute total variance of the sites.
cov_mtx <- (1/23) * crossprod(scaleO)
sum(diag(cov_mtx))
# Compute how much variance each PC captures
rowVars(t(score)) / sum(diag(cov_mtx))
# Check this matches the reported var expl.
```

```{r}
res <- cbind(res, PropVarExpl=summary(pcs)$importance["Proportion of Variance",1:7])
res
```

For each site, regress against CTs.
Get R^2 and multiply by var of that site.
Take expl var and divide by total var.
```{r}
lm_res <- NULL

var_betas <- validation_betas[ranked_list[1:t],]
# Switch to M values to do our linear modelling. 
var_m <- logit2(var_betas)

for(i in 1:nrow(var_m)){
  temp <- inner_join(data.frame(SampleName=colnames(var_m), Mval=var_m[i,]),
                     validation_counts, by="SampleName")
  fit <- lm(Mval ~ Gran + Mono + nRBC + NK + Bcell + CD4T + CD8T, data=temp)
  lm_res <- rbind(lm_res, data.frame(RSq=summary(fit)$r.squared, Var=var(temp$Mval)))
  if(i <= 5) { plot(temp$Mval, fit$fitted.values) }
}

lm_res <- lm_res %>% mutate(VarExpl=RSq * Var)
lm_res
# Total VarExpl
print("Variance Explained by Cell Proportions")
sum(lm_res$VarExpl) / sum(lm_res$Var)
```

```{r}
expl.by.ct <- sum(lm_res$VarExpl) / sum(lm_res$Var)

temp <- res %>% mutate(SharedVar=PropVarExpl*RsqCT)
temp <- temp %>% mutate(OtherVar=PropVarExpl- SharedVar,
                VarByCT=cumsum(SharedVar),
                Other=cumsum(OtherVar))

temp

temp %>% select(PC, VarByCT, Other) %>% gather(Source, Var.Expl, -PC) %>%
  ggplot(aes(PC, Var.Expl, fill=Source)) + geom_bar(stat="identity") + 
  ggtitle("Variance In PCs")

```