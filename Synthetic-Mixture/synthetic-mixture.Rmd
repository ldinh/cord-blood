---
title: "Synthetic Mixture Comparison"
author: "Louie Dinh"
date: '2016-11-29'
output: html_document
---

Purpose: Artificially mix the profiles in known proportions and diff them.

Initial data load.
```{r, include=FALSE}
## Libraries And Constants ##
library(FlowSorted.Blood.450k)
source("../lib/deconvolution-utils.R")

# The cell types that we're interested in.
ADULT_CELL_TYPES = c("CD8T","CD4T", "NK", "Bcell", "Mono", "Gran")
CELL_TYPES <- c("CD8T","CD4T", "NK", "Bcell", "Mono", "Gran", "nRBC")

## Load the adult data ##
adult_sorted_rgset <- FlowSorted.Blood.450k
adult_mset <- preprocessRaw(adult_sorted_rgset)
# Remove unwanted cell types.
adult_mset <- adult_mset[,adult_mset$CellType %in% ADULT_CELL_TYPES]

# Retrieve betas and our final PD.
adult_betas <- getBeta(adult_mset) %>% na.omit()
colnames(adult_betas) <- sub('+', 'T', colnames(adult_betas), fixed=T)
adult_pd <- pData(adult_mset)
adult_pd$Sample_Name <-  sub('+', 'T', adult_pd$Sample_Name, fixed=T)

## Load the Meaghan's sorted cord data ##
sheet <- read.450k.sheet("/Users/louie/Projects/cord-blood-deconvolution/Data-Raw/sorted")
msorted_rgset <- read.450k.exp(targets=sheet)

pData(msorted_rgset) <- pData(msorted_rgset) %>% select(-(WB_Perc_Neut:MC_Perc_CD8T),
                                                                -Basename,
                                                                -filenames)
pData(msorted_rgset)$CellType <- to_canonical_cell_types(v = msorted_rgset$Tissue,
                        cell_types = c("WB", "CBMC", "CD4T", "CD8T", "G", "Mo", "B", "NK", "RBC"),
                        canon_cell_types = c("WB", "CBMC", "CD4T", "CD8T", "Gran", "Mono", "Bcell", "NK", "nRBC"))
msorted_rgset$Tissue <- NULL

msorted_mset <- preprocessRaw(msorted_rgset)
# Get the WB samples.
msorted_wb_mset <- msorted_mset[,msorted_mset$CellType == "WB"]
msorted_wb_pd <- pData(msorted_wb_mset)
msorted_wb_betas <- getBeta(msorted_wb_mset)
colnames(msorted_wb_betas) <- paste("TS", msorted_wb_pd$Participant, sep="")
msorted_wb_counts <- read.csv("/Users/louie/Projects/cord-blood-deconvolution/Data/sorted-counts.csv",
                              header=T)
msorted_wb_counts <- msorted_wb_counts %>%
                      mutate(Gran=Neutro+Eo+Baso) %>%
                      select(-Neutro, -Baso, -Eo)
msorted_wb_counts <- gather(msorted_wb_counts, CellType, True.Prop, -SampleName)
msorted_wb_counts$True.Prop <- msorted_wb_counts$True.Prop / 100
# Get the sorted samples.
msorted_mset <- msorted_mset[,msorted_mset$CellType %in% CELL_TYPES]
msorted_betas <- getBeta(msorted_mset)
msorted_pd <- pData(msorted_mset)

## Load Meagan's validation data. ##
validation_sheet <- read.450k.sheet("/Users/louie/Projects/cord-blood-deconvolution/Data-Raw/validation/")
validation_rgset <- read.450k.exp(targets=validation_sheet)
# Load in the counts
validation_counts <- read.csv("/Users/louie/Projects/cord-blood-deconvolution/Data/validation-counts.csv", header=T)
validation_counts <- gather(validation_counts, CellType, True.Prop, -SampleName)
validation_counts$True.Prop <- validation_counts$True.Prop / 100
validation_counts$CellType <- to_canonical_cell_types(validation_counts$CellType,
                        c("CD8T", "CD4T", "NK", "Bcell", "Monocytes", "Granulocytes", "nRBCs"),
                        c("CD8T","CD4T","NK","Bcell","Mono", "Gran", "nRBC"))

validation_cbcs_raw <- read.csv("/Users/louie/Projects/cord-blood-deconvolution/Data/raw-validation-cbcs.csv", header=T)
validation_cbcs <- gather(validation_cbcs_raw, CellType, Count, -SampleName)
total_cell_counts <- validation_cbcs %>% group_by(SampleName) %>% summarize(TotalCount=sum(Count))
validation_cbcs <- inner_join(validation_cbcs, total_cell_counts, by="SampleName") %>%
  mutate(True.Prop=Count/TotalCount) %>% select(SampleName, CellType, True.Prop)
validation_gran <- validation_cbcs %>% filter(CellType %in% c("Neut", "Eos", "Baso")) %>% 
  group_by(SampleName) %>% summarize(CellType="Gran", True.Prop=sum(True.Prop))
validation_other <- validation_cbcs %>% filter(!(CellType %in% c("Neut", "Eos", "Baso")))
validation_cbcs <- rbind(validation_gran, validation_other)

validation_cbcs_norbc <- gather(validation_cbcs_raw, CellType, Count, -SampleName) %>% filter(CellType != "nRBC")
total_cell_counts <- validation_cbcs_norbc %>% group_by(SampleName) %>% summarize(TotalCount=sum(Count))
validation_cbcs_norbc <- inner_join(validation_cbcs_norbc, total_cell_counts, by="SampleName") %>%
  mutate(True.Prop=Count/TotalCount) %>% select(SampleName, CellType, True.Prop)
validation_gran <- validation_cbcs_norbc %>% filter(CellType %in% c("Neut", "Eos", "Baso")) %>% 
  group_by(SampleName) %>% summarize(CellType="Gran", True.Prop=sum(True.Prop))
validation_other <- validation_cbcs_norbc %>% filter(!(CellType %in% c("Neut", "Eos", "Baso")))
validation_cbcs_norbc <- rbind(validation_gran, validation_other)

# Rename the samples in the rgset to match our counts data.
sample2array <- read.csv("/Users/louie/Projects/cord-blood-deconvolution/Data/validation-sample_to_array.csv", header=T)
# Map of array_ids to sample_ids
idx <- match(sampleNames(validation_rgset), sample2array$ArrayName)
sampleNames(validation_rgset) <- sample2array$SampleName[idx]
# Get the Betas
validation_mset <- preprocessRaw(validation_rgset)
validation_betas <- getBeta(validation_mset)
validation_pd <- pData(validation_mset)

# Extra libraries for this analysis.
library(gplots)
library(RColorBrewer)
```

```{r}
# Construct the profile.
dm_probes_by_ct <- select_probes_t_separation_by_ct(betas=msorted_betas,
                                         cell_type_ind=msorted_pd$CellType,
                                         cell_types=CELL_TYPES, N=100)

probes <- unlist(dm_probes_by_ct)
cord_profile <- construct_profiles(betas=msorted_betas,
                                   probes=probes,
                                   cell_types=CELL_TYPES,
                                   cell_type_ind=msorted_pd$CellType)

synthetic_validation_betas <- matrix(NA, nrow=nrow(cord_profile),
                                     ncol=ncol(validation_betas))
for(i in 1:ncol(validation_betas)){
  sample_name <- colnames(validation_betas)[i]
  sample_props <- validation_counts %>% filter(SampleName == sample_name) %>%
    spread(CellType, True.Prop)
  sample_props <- as.numeric(sample_props[,colnames(cord_profile)])
  sample_beta <- cord_profile %*% sample_props
  synthetic_validation_betas[,i] <- sample_beta
}
colnames(synthetic_validation_betas) <- colnames(validation_betas)
rownames(synthetic_validation_betas) <- rownames(cord_profile)

est_cell_counts <- minfi:::projectCellType(synthetic_validation_betas[rownames(cord_profile),],
                                           cord_profile,
                                           nonnegative = T,
                                           lessThanOne = T)

# Plot against the counts.
validation_data <- compare_estimation_to_truth(est_cell_counts, validation_counts)
plot_deconv_accuracy(validation_data)

diff_betas <- validation_betas[rownames(cord_profile),] - synthetic_validation_betas

heatmap.2(diff_betas, density.info = "none", trace="none", col=brewer.pal(10, "Spectral"))

# Looks like a big block has a much bigger repr in the truth than in synthetic.
mean_diffs <- rowMeans(diff_betas)
ordered_mean_diffs <- mean_diffs[order(mean_diffs, decreasing = T)]
ordered_probes <- names(ordered_mean_diffs)

sum(ordered_probes[1:200] %in% dm_probes_by_ct$Gran)
sum(ordered_probes[1:200] %in% dm_probes_by_ct$Mono)
sum(ordered_probes[1:200] %in% dm_probes_by_ct$CD4T)
sum(ordered_probes[1:200] %in% dm_probes_by_ct$CD8T)
sum(ordered_probes[1:200] %in% dm_probes_by_ct$Bcell)
sum(ordered_probes[1:200] %in% dm_probes_by_ct$NK)
sum(ordered_probes[1:200] %in% dm_probes_by_ct$nRBC)

# 100 of the higher than expected probes belong to Gran, 41 belong to Mono, 35 belong to CD4T
# There are only 14 overcounted, 6, 2, 2 in CD8T, Bcell, NK and nRBC.

# Plot the over/under at each probe/cell type.
ordered_mdiffs_tidy <- data.frame(Probe=names(ordered_mean_diffs), Beta=ordered_mean_diffs)
probe_by_ct <- gather(data.frame(dm_probes_by_ct), CellType, Probe)
dat <- inner_join(ordered_mdiffs_tidy, probe_by_ct, by="Probe")
dat$Probe <- factor(dat$Probe, levels=ordered_probes)

ggplot(dat, aes(Probe, Beta, fill=CellType)) + geom_bar(stat="identity")
ggplot(dat %>% filter(!(CellType %in% c("Gran", "nRBC", "NK"))), aes(Probe, Beta, fill=CellType)) + geom_bar(stat="identity")
ggplot(dat %>% filter((CellType %in% c("Gran", "Mono"))), aes(Probe, Beta, fill=CellType)) + geom_bar(stat="identity")

# Good results, as we expect them.
ggplot(dat, aes(Probe, Beta, fill=CellType)) + geom_bar(stat="identity") + facet_wrap( ~ CellType)

heatmap.2(diff_betas[c(dm_probes_by_ct$Gran, dm_probes_by_ct$NK),],
          density.info = "none", trace="none", col=brewer.pal(10, "Spectral"))

heatmap.2(diff_betas, density.info = "none", trace="none", col=brewer.pal(10, "Spectral"),
          dendrogram = "none", Rowv = FALSE, Colv = FALSE)

# Weird block representing mismatch between mixture and actual
heatmap.2(diff_betas[dm_probes_by_ct$Gran,], density.info = "none",
          trace="none", col=brewer.pal(10, "Spectral"),
          dendrogram = "none", Rowv = FALSE, Colv = FALSE, main = "Gran")
heatmap.2(diff_betas[dm_probes_by_ct$Mono,], density.info = "none",
          trace="none", col=brewer.pal(10, "Spectral"),
          dendrogram = "none", Rowv = FALSE, Colv = FALSE, main="Mono")
heatmap.2(diff_betas[dm_probes_by_ct$Bcell,], density.info = "none",
          trace="none", col=brewer.pal(10, "Spectral"),
          dendrogram = "none", Rowv = FALSE, Colv = FALSE, main="Bcell")
heatmap.2(diff_betas[dm_probes_by_ct$CD4T,], density.info = "none",
          trace="none", col=brewer.pal(10, "Spectral"),
          dendrogram = "none", Rowv = FALSE, Colv = FALSE, main="CD4T")
heatmap.2(diff_betas[dm_probes_by_ct$CD8T,], density.info = "none",
          trace="none", col=brewer.pal(10, "Spectral"),
          dendrogram = "none", Rowv = FALSE, Colv = FALSE, main="CD8T")
heatmap.2(diff_betas[dm_probes_by_ct$nRBC,], density.info = "none",
          trace="none", col=brewer.pal(10, "Spectral"),
          dendrogram = "none", Rowv = FALSE, Colv = FALSE, main="nRBC")

# Cool histogram.
gather(data.frame(diff_betas), SampleName, Beta) %>% ggplot(aes(Beta, col=SampleName)) + geom_histogram() + facet_wrap(~ SampleName) + geom_vline(xintercept = 0)
```

Recenter
```{r}
probes <- rownames(cord_profile)
validation_betas_rc <- recenter_betas(msorted_wb_betas, validation_betas, probes=probes)
diff_betas_rc <- validation_betas_rc[rownames(cord_profile),] - synthetic_validation_betas

heatmap.2(diff_betas_rc, density.info = "none", trace="none", col=brewer.pal(10, "Spectral"),
          dendrogram = "none", Rowv = FALSE, Colv = FALSE)
# Weird block representing mismatch between mixture and actual
heatmap.2(diff_betas_rc[dm_probes_by_ct$Gran,], density.info = "none",
          trace="none", col=brewer.pal(10, "Spectral"),
          dendrogram = "none", Rowv = FALSE, Colv = FALSE, main = "Gran")
heatmap.2(diff_betas_rc[dm_probes_by_ct$Mono,], density.info = "none",
          trace="none", col=brewer.pal(10, "Spectral"),
          dendrogram = "none", Rowv = FALSE, Colv = FALSE, main="Mono")
heatmap.2(diff_betas_rc[dm_probes_by_ct$Bcell,], density.info = "none",
          trace="none", col=brewer.pal(10, "Spectral"),
          dendrogram = "none", Rowv = FALSE, Colv = FALSE, main="Bcell")
heatmap.2(diff_betas_rc[dm_probes_by_ct$CD4T,], density.info = "none",
          trace="none", col=brewer.pal(10, "Spectral"),
          dendrogram = "none", Rowv = FALSE, Colv = FALSE, main="CD4T")
heatmap.2(diff_betas_rc[dm_probes_by_ct$CD8T,], density.info = "none",
          trace="none", col=brewer.pal(10, "Spectral"),
          dendrogram = "none", Rowv = FALSE, Colv = FALSE, main="CD8T")
heatmap.2(diff_betas_rc[dm_probes_by_ct$nRBC,], density.info = "none",
          trace="none", col=brewer.pal(10, "Spectral"),
          dendrogram = "none", Rowv = FALSE, Colv = FALSE, main="nRBC")

# Aggregate cell types.
# Looks like a big block has a much bigger repr in the truth than in synthetic.
mean_diffs <- rowMeans(diff_betas_rc)
ordered_mean_diffs <- mean_diffs[order(mean_diffs, decreasing = T)]
ordered_probes <- names(ordered_mean_diffs)

# Plot the over/under at each probe/cell type.
ordered_mdiffs_tidy <- data.frame(Probe=names(ordered_mean_diffs), Beta=ordered_mean_diffs)
probe_by_ct <- gather(data.frame(dm_probes_by_ct), CellType, Probe)
dat <- inner_join(ordered_mdiffs_tidy, probe_by_ct, by="Probe")
dat$Probe <- factor(dat$Probe, levels=ordered_probes)

ggplot(dat, aes(Probe, Beta, fill=CellType)) + geom_bar(stat="identity")
ggplot(dat, aes(Probe, Beta, fill=CellType)) + geom_bar(stat="identity") + facet_wrap( ~ CellType)

# Aggregate the cell types.
dat_l <- dat %>% filter(CellType %in% c("CD4T", "CD8T", "NK", "Bcell")) %>% mutate(CellType = "Lymph")
dat_gmr <- dat %>% filter(!(CellType %in% c("CD4T", "CD8T", "NK", "Bcell"))) 
dat <- rbind(dat_l, dat_gmr)
ggplot(dat, aes(Probe, Beta, fill=CellType)) + geom_bar(stat="identity") + facet_wrap( ~ CellType)


```

More Probes
```{r}
# Construct the profile.
dm_probes_by_ct <- select_probes_t_separation_by_ct(betas=msorted_betas,
                                                    cell_type_ind=msorted_pd$CellType,
                                                    cell_types=CELL_TYPES, N=100)

probes <- unlist(dm_probes_by_ct)
cord_profile <- construct_profiles(betas=msorted_betas,
                                   probes=probes,
                                   cell_types=CELL_TYPES,
                                   cell_type_ind=msorted_pd$CellType)

synthetic_validation_betas <- matrix(NA, nrow=nrow(cord_profile),
                                     ncol=ncol(validation_betas))
for(i in 1:ncol(validation_betas)){
  sample_name <- colnames(validation_betas)[i]
  sample_props <- validation_counts %>% filter(SampleName == sample_name) %>%
    spread(CellType, True.Prop)
  sample_props <- as.numeric(sample_props[,colnames(cord_profile)])
  sample_beta <- cord_profile %*% sample_props
  synthetic_validation_betas[,i] <- sample_beta
}
colnames(synthetic_validation_betas) <- colnames(validation_betas)
rownames(synthetic_validation_betas) <- rownames(cord_profile)

est_cell_counts <- minfi:::projectCellType(validation_betas[rownames(cord_profile),],
                                           cord_profile,
                                           nonnegative = T,
                                           lessThanOne = T)
# Plot against the counts.
validation_data <- compare_estimation_to_truth(est_cell_counts, validation_counts)
plot_deconv_accuracy(validation_data, plot_scales="free")

est_cell_counts <- minfi:::projectCellType(synthetic_validation_betas[rownames(cord_profile),],
                                           cord_profile,
                                           nonnegative = T,
                                           lessThanOne = T)
# Plot against the counts.
validation_data <- compare_estimation_to_truth(est_cell_counts, validation_counts)
plot_deconv_accuracy(validation_data)

diff_betas <- validation_betas[rownames(cord_profile),] - synthetic_validation_betas

heatmap.2(diff_betas, density.info = "none", trace="none", col=brewer.pal(10, "Spectral"))
# Looks like a big block has a much bigger repr in the truth than in synthetic.
mean_diffs <- rowMeans(diff_betas)
ordered_mean_diffs <- mean_diffs[order(mean_diffs, decreasing = T)]
ordered_probes <- names(ordered_mean_diffs)

# Plot the over/under at each probe/cell type.
ordered_mdiffs_tidy <- data.frame(Probe=names(ordered_mean_diffs), Beta=ordered_mean_diffs)
probe_by_ct <- gather(data.frame(dm_probes_by_ct), CellType, Probe)
dat <- inner_join(ordered_mdiffs_tidy, probe_by_ct, by="Probe")

dat$Probe <- factor(dat$Probe, levels=ordered_probes)
ggplot(dat, aes(Probe, Beta, fill=CellType)) + geom_bar(stat="identity")
ggplot(dat, aes(Probe, Beta, fill=CellType)) + geom_bar(stat="identity") + facet_wrap( ~ CellType)

# Get the diffs across the top 10000 sites in Gran.
# Construct the profile.
dm_probes_gran <- select_probes_t_separation(betas=msorted_betas,
                                             target_ct="Gran",  
                                             cell_type_ind=msorted_pd$CellType,
                                             cell_types=CELL_TYPES, N=10000)

# Construct the profile for Gran at the crazy long list of sites.
gran_profile <-  construct_profiles(betas=msorted_betas,
                                   probes=dm_probes_gran,
                                   cell_types=CELL_TYPES,
                                   cell_type_ind=msorted_pd$CellType)

synthetic_validation_betas <- matrix(NA, nrow=length(dm_probes_gran),
                                     ncol=ncol(validation_betas))
for(i in 1:ncol(validation_betas)){
  sample_name <- colnames(validation_betas)[i]
  sample_props <- validation_counts %>% filter(SampleName == sample_name) %>%
    spread(CellType, True.Prop)
  sample_props <- as.numeric(sample_props[,colnames(cord_profile)])
  sample_beta <- gran_profile %*% sample_props
  synthetic_validation_betas[,i] <- sample_beta
}
colnames(synthetic_validation_betas) <- colnames(validation_betas)
rownames(synthetic_validation_betas) <- rownames(gran_profile)

diff_betas <- validation_betas[dm_probes_gran,] - synthetic_validation_betas
mean_diffs <- rowMeans(diff_betas)
ordered_mean_diffs <- mean_diffs[order(mean_diffs, decreasing = T)]
ordered_probes <- names(ordered_mean_diffs)
ordered_mdiffs_tidy <- data.frame(Probe=names(ordered_mean_diffs), Beta=ordered_mean_diffs)
probe_by_ct <- gather(data.frame(dm_probes_by_ct), CellType, Probe)
dat <- inner_join(ordered_mdiffs_tidy, probe_by_ct, by="Probe")
dat$Probe <- factor(dat$Probe, levels=ordered_probes)

ggplot(dat, aes(Probe, Beta, fill=CellType)) + geom_bar(stat="identity")
```
