---
title: "Fix-nRBC-Counts"
author: "Louie Dinh"
date: '2016-12-05'
output: html_document
---
Hypothesis: Wrong nRBC counts is causing scaling issues on large cell types.

Setup
-----
Do some setup.

Set global options to supress noisy output.
```{r global_options, include=FALSE}
knitr::opts_chunk$set(warning=FALSE, message=FALSE)
```

Initial data load.
```{r, include=FALSE}
## Libraries And Constants ##
library(FlowSorted.Blood.450k)
source("../lib/deconvolution-utils.R")

# The cell types that we're interested in.
ADULT_CELL_TYPES = c("CD8T","CD4T", "NK", "Bcell", "Mono", "Gran")
CELL_TYPES <- c("CD8T","CD4T", "NK", "Bcell", "Mono", "Gran", "nRBC")

## Load the adult data ##
adult_sorted_rgset <- FlowSorted.Blood.450k
adult_mset <- preprocessRaw(adult_sorted_rgset)
# Remove unwanted cell types.
adult_mset <- adult_mset[,adult_mset$CellType %in% ADULT_CELL_TYPES]

# Retrieve betas and our final PD.
adult_betas <- getBeta(adult_mset) %>% na.omit()
colnames(adult_betas) <- sub('+', 'T', colnames(adult_betas), fixed=T)
adult_pd <- pData(adult_mset)
adult_pd$Sample_Name <-  sub('+', 'T', adult_pd$Sample_Name, fixed=T)

## Load the Meaghan's sorted cord data ##
sheet <- read.450k.sheet("/Users/louie/Projects/cord-blood-deconvolution/Data-Raw/sorted")
msorted_rgset <- read.450k.exp(targets=sheet)

pData(msorted_rgset) <- pData(msorted_rgset) %>% select(-(WB_Perc_Neut:MC_Perc_CD8T),
                                                                -Basename,
                                                                -filenames)
pData(msorted_rgset)$CellType <- to_canonical_cell_types(v = msorted_rgset$Tissue,
                        cell_types = c("WB", "CBMC", "CD4T", "CD8T", "G", "Mo", "B", "NK", "RBC"),
                        canon_cell_types = c("WB", "CBMC", "CD4T", "CD8T", "Gran", "Mono", "Bcell", "NK", "nRBC"))
msorted_rgset$Tissue <- NULL

msorted_mset <- preprocessRaw(msorted_rgset)
# Get the WB samples.
msorted_wb_mset <- msorted_mset[,msorted_mset$CellType == "WB"]
msorted_wb_pd <- pData(msorted_wb_mset)
msorted_wb_betas <- getBeta(msorted_wb_mset)
colnames(msorted_wb_betas) <- paste("TS", msorted_wb_pd$Participant, sep="")
msorted_wb_counts <- read.csv("/Users/louie/Projects/cord-blood-deconvolution/Data/sorted-counts.csv",
                              header=T)
msorted_wb_counts <- msorted_wb_counts %>%
                      mutate(Gran=Neutro+Eo+Baso) %>%
                      select(-Neutro, -Baso, -Eo)
msorted_wb_counts <- gather(msorted_wb_counts, CellType, True.Prop, -SampleName)
msorted_wb_counts$True.Prop <- msorted_wb_counts$True.Prop / 100
# Get the sorted samples.
msorted_mset <- msorted_mset[,msorted_mset$CellType %in% CELL_TYPES]
msorted_betas <- getBeta(msorted_mset)
msorted_pd <- pData(msorted_mset)

## Load Meagan's validation data. ##
validation_sheet <- read.450k.sheet("/Users/louie/Projects/cord-blood-deconvolution/Data-Raw/validation/")
validation_rgset <- read.450k.exp(targets=validation_sheet)
# Load in the counts
validation_counts <- read.csv("/Users/louie/Projects/cord-blood-deconvolution/Data/validation-counts.csv", header=T)
validation_counts <- gather(validation_counts, CellType, True.Prop, -SampleName)
validation_counts$True.Prop <- validation_counts$True.Prop / 100
validation_counts$CellType <- to_canonical_cell_types(validation_counts$CellType,
                        c("CD8T", "CD4T", "NK", "Bcell", "Monocytes", "Granulocytes", "nRBCs"),
                        c("CD8T","CD4T","NK","Bcell","Mono", "Gran", "nRBC"))

validation_cbcs_raw <- read.csv("/Users/louie/Projects/cord-blood-deconvolution/Data/raw-validation-cbcs.csv", header=T)
validation_cbcs <- gather(validation_cbcs_raw, CellType, Count, -SampleName)
total_cell_counts <- validation_cbcs %>% group_by(SampleName) %>% summarize(TotalCount=sum(Count))
validation_cbcs <- inner_join(validation_cbcs, total_cell_counts, by="SampleName") %>%
  mutate(True.Prop=Count/TotalCount) %>% select(SampleName, CellType, True.Prop)
validation_gran <- validation_cbcs %>% filter(CellType %in% c("Neut", "Eos", "Baso")) %>% 
  group_by(SampleName) %>% summarize(CellType="Gran", True.Prop=sum(True.Prop))
validation_other <- validation_cbcs %>% filter(!(CellType %in% c("Neut", "Eos", "Baso")))
validation_cbcs <- rbind(validation_gran, validation_other)

validation_cbcs_norbc <- gather(validation_cbcs_raw, CellType, Count, -SampleName) %>% filter(CellType != "nRBC")
total_cell_counts <- validation_cbcs_norbc %>% group_by(SampleName) %>% summarize(TotalCount=sum(Count))
validation_cbcs_norbc <- inner_join(validation_cbcs_norbc, total_cell_counts, by="SampleName") %>%
  mutate(True.Prop=Count/TotalCount) %>% select(SampleName, CellType, True.Prop)
validation_gran <- validation_cbcs_norbc %>% filter(CellType %in% c("Neut", "Eos", "Baso")) %>% 
  group_by(SampleName) %>% summarize(CellType="Gran", True.Prop=sum(True.Prop))
validation_other <- validation_cbcs_norbc %>% filter(!(CellType %in% c("Neut", "Eos", "Baso")))
validation_cbcs_norbc <- rbind(validation_gran, validation_other)

# Rename the samples in the rgset to match our counts data.
sample2array <- read.csv("/Users/louie/Projects/cord-blood-deconvolution/Data/validation-sample_to_array.csv", header=T)
# Map of array_ids to sample_ids
idx <- match(sampleNames(validation_rgset), sample2array$ArrayName)
sampleNames(validation_rgset) <- sample2array$SampleName[idx]
# Get the Betas
validation_mset <- preprocessRaw(validation_rgset)
validation_betas <- getBeta(validation_mset)
validation_pd <- pData(validation_mset)
```

Figure out the scaling constant that we are getting nRBC wrong by.
```{r}
# Construct the profile.
dm_probes_by_ct <- select_probes_t_separation_by_ct(betas=msorted_betas,
                                         cell_type_ind=msorted_pd$CellType,
                                         cell_types=CELL_TYPES, N=100)

probes <- unlist(dm_probes_by_ct)
cord_profile <- construct_profiles(betas=msorted_betas, probes=probes, cell_types=CELL_TYPES, 
                   cell_type_ind=msorted_pd$CellType)

est_cell_counts <- minfi:::projectCellType(validation_betas[rownames(cord_profile),],
                                           cord_profile,
                                           nonnegative = T,
                                           lessThanOne = T)


# Plot against the counts.
validation_data <- compare_estimation_to_truth(est_cell_counts, validation_counts)
plot_deconv_accuracy(validation_data)

nrbc_ests <- validation_data %>% filter(CellType == "nRBC") %>% filter(SampleName != "TS234")
ggplot(nrbc_ests, aes(True.Prop, Est.Prop)) + geom_point() + stat_smooth(method="lm")
fit <- lm(Est.Prop ~ True.Prop, data=nrbc_ests)
summary(fit)
plot(fit$fitted.values, fit$residuals)

nrbc_scaling_factor <- fit$coefficients["True.Prop"]

# Rescale all other cell types in light of the new nRBCs
counts_nrbc <- validation_counts %>% filter(CellType == "nRBC") %>% 
  select(SampleName, CellType, True.Prop) %>%
  mutate(True.Prop=True.Prop*nrbc_scaling_factor)
counts_other <- counts_nrbc %>% mutate(Other.Prop=1-True.Prop) %>% select(SampleName, Other.Prop)
total_other <- validation_counts %>% filter(CellType != "nRBC") %>% 
               group_by(SampleName) %>% summarize(Total=sum(True.Prop))
# Rescale all other cell types in light of the new nRBCs
total_other <- validation_counts %>% filter(CellType != "nRBC") %>% 
               group_by(SampleName) %>% summarize(Total=sum(True.Prop))
counts_other_scaled <- inner_join(validation_counts %>%
                                filter(CellType != "nRBC"), total_other) %>%
                      mutate(True.Prop.Scaled=True.Prop / Total) %>% 
                      select(-True.Prop, -Total)
rescaled_validation_counts_other <- inner_join(counts_other_scaled, counts_other,
                                         by="SampleName") %>%
                              mutate(True.Prop=True.Prop.Scaled * Other.Prop) %>% 
                              select(SampleName, CellType, True.Prop)

rescaled_validation_counts <- rbind(rescaled_validation_counts_other, counts_nrbc)


validation_data <- compare_estimation_to_truth(est_cell_counts,
                                               rescaled_validation_counts)
plot_deconv_accuracy(validation_data)
```

Conclusion: Not nRBC scaling that causes it. Although fixing them does improve errors on most cell types a bit.


Plot the rescaled counts.
```{r}
validation_betas_rc <- recenter_betas(reference_betas = msorted_wb_betas,
                                validation_betas = validation_betas,
                                probes=intersect(row.names(validation_betas),
                                                 row.names(validation_betas)))
est_cell_counts <- minfi:::projectCellType(validation_betas_rc[rownames(cord_profile),],
                                           cord_profile,
                                           nonnegative = T,
                                           lessThanOne = T)
validation_data <- compare_estimation_to_truth(est_cell_counts, rescaled_validation_counts)
plot_deconv_accuracy(validation_data)

# Plot against the CBCs
est_cbcs <- cbind(est_cell_counts, Lymph=(est_cell_counts[,"CD4T"] +
                               est_cell_counts[,"CD8T"] +
                               est_cell_counts[,"NK"] +
                               est_cell_counts[,"Bcell"]))
rescaled_validation_cbcs_gmr <- rescaled_validation_counts %>% 
  filter(CellType %in% c("Gran", "Mono", "nRBC"))
rescaled_validation_cbcs_lymph <- rescaled_validation_counts %>% 
  filter(CellType %in% c("CD4T", "CD8T", "NK", "Bcell")) %>% group_by(SampleName) %>%
  summarize(True.Prop=sum(True.Prop), CellType="Lymph")
rescaled_validation_cbcs <- rbind(rescaled_validation_cbcs_gmr,  rescaled_validation_cbcs_lymph)

cbcs_data <- compare_estimation_to_truth(est_cbcs, rescaled_validation_cbcs)
plot_deconv_accuracy(cbcs_data)


```
```