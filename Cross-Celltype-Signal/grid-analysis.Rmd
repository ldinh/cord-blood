---
title: "Grid-Analysis"
author: "Louie Dinh"
date: "October 29, 2016"
output: html_document
---

Purpose: Analyze the cross celltype grid.

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
library(RColorBrewer)
library(ggplot2)
library(tidyr)
```

```{r}
adult_grid <- read.csv("auc-grid-adult.csv", header=T)
row.names(adult_grid) <- adult_grid$CellType
adult_grid$CellType <- NULL
adult_grid <- as.matrix(adult_grid)
names <- rev(colnames(adult_grid))
adult_grid <- adult_grid[,names]

heatmap(adult_grid, col=brewer.pal(5, "RdBu"),
        ylab="Profile", xlab="Predicted", Rowv = NA, Colv = NA)

data <- data.frame(adult_grid, ProfileCellType=row.names(adult_grid))
data <- gather(data, PredictedCellType, AUC, -ProfileCellType)
data$ProfileCellType <- factor(as.character(data$ProfileCellType),
                               levels=names) 
data$PredictedCellType <- factor(as.character(data$PredictedCellType),
                               levels=names) 

ggplot(data, aes(PredictedCellType, ProfileCellType)) +
    geom_tile(aes(fill = AUC)) + 
    geom_text(aes(fill = data$AUC, label = round(data$AUC, 2))) +
    scale_fill_gradient(limits=c(.4,1), low="red", high="white") +
    ggtitle("Adult") 
```

```{r}
cord_grid <- read.csv("auc-grid-cord.csv", header=T)
row.names(cord_grid) <- cord_grid$CellType
cord_grid$CellType <- NULL
cord_grid <- as.matrix(cord_grid)
names <- rev(colnames(cord_grid))
cord_grid <- cord_grid[,names]

heatmap(cord_grid, col=brewer.pal(5, "RdBu"),
        ylab="Profile", xlab="Predicted", Rowv=NA, Colv=NA)

data <- data.frame(cord_grid, ProfileCellType=row.names(cord_grid))
data <- gather(data, PredictedCellType, AUC, -ProfileCellType)
data$ProfileCellType <- factor(as.character(data$ProfileCellType),
                               levels=names) 
data$PredictedCellType <- factor(as.character(data$PredictedCellType),
                               levels=names) 

ggplot(data, aes(PredictedCellType, ProfileCellType)) +
    geom_tile(aes(fill = AUC)) + 
    geom_text(aes(fill = data$AUC, label = round(data$AUC, 2))) +
    scale_fill_gradient(limits=c(.4,1), low="red", high="white") +
    ggtitle("Cord")
```