---
title: "CD4T-vs-All"
author: "Louie Dinh"
date: "September 12, 2016"
output: html_document
---

Purpose:
Let's try to accurately predict Granulocytes. We will try to do this by eliminating the cell types that are similar, but are trivial in size. Also, we will find only *very* good probes. 

```{r setup, include=FALSE}
library(FlowSorted.Blood.450k)
source("../lib/deconvolution-utils.R")

# The cell types that we're interested in.
CELL_TYPES = c("CD8T","CD4T", "NK", "Bcell", "Mono", "Gran", "nRBC")
ADULT_CELL_TYPES = c("CD8T","CD4T", "NK", "Bcell", "Mono", "Gran")


## Load the adult data
adult_sorted_rgset <- FlowSorted.Blood.450k
adult_mset <- preprocessRaw(adult_sorted_rgset)
# Remove unwanted cell types.
adult_mset <- adult_mset[,adult_mset$CellType %in% ADULT_CELL_TYPES]

# Retrieve betas and our final PD.
adult_betas <- getBeta(adult_mset) %>% na.omit()
colnames(adult_betas) <- sub('+', 'T', colnames(adult_betas), fixed=T)
adult_pd <- pData(adult_mset)


## Load the sorted cord data.
sheet <- read.450k.sheet("/Users/louie/Projects/cord-blood-deconvolution/Data-Raw/sorted")
sorted_cord_rgset <- read.450k.exp(targets=sheet)

pData(sorted_cord_rgset) <- pData(sorted_cord_rgset) %>% select(-(WB_Perc_Neut:MC_Perc_CD8T),
                                                                -Basename,
                                                                -filenames)
pData(sorted_cord_rgset)$CellType <- to_canonical_cell_types(v = sorted_cord_rgset$Tissue,
                        cell_types = c("WB", "CBMC", "CD4T", "CD8T", "G", "Mo", "B", "NK", "RBC"),
                        canon_cell_types = c("WB", "CBMC", "CD4T", "CD8T", "Gran", "Mono", "Bcell", "NK", "nRBC"))
sorted_cord_rgset$Tissue <- NULL

sorted_cord_mset <- preprocessRaw(sorted_cord_rgset)
sorted_cord_mset <- sorted_cord_mset[,sorted_cord_mset$CellType %in% CELL_TYPES]
sorted_cord_betas <- getBeta(sorted_cord_mset)
sorted_cord_pd <- pData(sorted_cord_mset)


# Load the validation data.
validation_rgset <- read.450k.exp(base = "/Users/louie/Projects/cord-blood-deconvolution/Data-Raw/validation", recursive = TRUE)
# Load in the counts
validation_counts <- read.csv("/Users/louie/Projects/cord-blood-deconvolution/Data/validation-counts.csv", header=T)
validation_counts <- gather(validation_counts, CellType, True.Prop, -SampleName)
validation_counts$True.Prop <- validation_counts$True.Prop / 100
validation_counts$CellType <- to_canonical_cell_types(validation_counts$CellType,
                        c("CD8T", "CD4T", "NK", "Bcell", "Monocytes", "Granulocytes", "nRBCs"),
                        c("CD8T","CD4T","NK","Bcell","Mono", "Gran", "nRBC"))

# Rename the samples in the rgset to match our counts data.
sample2array <- read.csv("/Users/louie/Projects/cord-blood-deconvolution/Data/validation-sample_to_array.csv", header=T)
# Map of array_ids to sample_ids
idx <- match(sampleNames(validation_rgset), sample2array$ArrayName)
sampleNames(validation_rgset) <- sample2array$SampleName[idx]
# Get the Betas
validation_mset <- preprocessRaw(validation_rgset)
validation_betas <- getBeta(validation_mset)
```

Probe selection for cd4t, here we use adult as the initial candidate filter.
```{r}
adult_dm_probes_by_ct <- select_probes_t_by_ct(adult_betas=adult_betas,
                                         target_ct="CD4T",
                                         cell_type_ind=adult_pd$CellType,
                                         cell_types=CELL_TYPES,
                                         N=50)

adult_up_probes <- adult_dm_probes_by_ct[["CD4T"]]$Up
adult_down_probes <- adult_dm_probes_by_ct[["CD4T"]]$Down
cord_betas_updown <- sorted_cord_betas[c(adult_down_probes, adult_up_probes),]

dm_probes_cd4 <- select_probes_t(betas=cord_betas_updown,
                             target_ct="CD4T",
                             cell_type_ind=sorted_cord_pd$CellType,
                             cell_types=CELL_TYPES,
                             N=50)

plot_probes(betas = sorted_cord_betas, probes = dm_probes_cd4$Down, group_labels = sorted_cord_pd$CellType)
plot_probes(betas = sorted_cord_betas, probes = dm_probes_cd4$Up, group_labels = sorted_cord_pd$CellType)
```

Try not merging the other profiles.
```{r}
probes <- unlist(dm_probes_cd4)

cord_profile <- construct_profiles(betas=sorted_cord_betas,
                               probes=probes,
                               cell_types=CELL_TYPES,
                               cell_type_ind=sorted_cord_pd$CellType)

# Do the deconvolution
est_cell_counts <- minfi:::projectCellType(validation_betas[probes,], cord_profile, nonnegative = T, lessThanOne = T)
validation_data <- compare_estimation_to_truth(est_cell_counts, validation_counts)
plot_deconv_accuracy(validation_data)
```

Merge the other cell types, and construct the profile.
```{r}
probes <- unlist(dm_probes_cd4)

merged_celltype_ind <- sorted_cord_pd$CellType
merged_celltype_ind[merged_celltype_ind != "CD4T"] <- "Other"

merged_cord_profile <- construct_profiles(betas=sorted_cord_betas,
                               probes=probes,
                               cell_types=c("CD4T", "Other"),
                               cell_type_ind=merged_celltype_ind)

merged_validation_counts_cd4 <- validation_counts %>% filter(CellType == "CD4T")
merged_validation_counts_other <- validation_counts %>%
                                  filter(CellType != "CD4T") %>%
                                  group_by(SampleName) %>%
                                  summarize(CellType = "Other", True.Prop=sum(True.Prop))
merged_validation_counts <- rbind(merged_validation_counts_cd4, merged_validation_counts_other)


# Do the deconvolution
est_cell_counts <- minfi:::projectCellType(validation_betas[probes,], merged_cord_profile, nonnegative = T, lessThanOne = T)
validation_data <- compare_estimation_to_truth(est_cell_counts, merged_validation_counts)
plot_deconv_accuracy(validation_data)
```
