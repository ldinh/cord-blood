Insights:
 - Lumping together the other cell types is not usually that deterimental.
 - Fewer good probes is much more powerful than many bad probes.

TODOs:
 - Test all subsets of probes in decreasing order of significance.
 - Use Bakulski data ot control for false positives.
 - Why are the least significant up probes better than the most significant up probes?
