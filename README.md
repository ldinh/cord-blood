Adult-Recalibration - Use adult references to select DMRs. Then select a subset in cord to use for deconvolution.
Adult-Sig-Baseline - Use the adult references to deconvolute cord blood.
Augment-Cord - Use both Kobor and Bakulski data to do deconvolution. 
Bakulski-Only - Use only Bakulski reference data to do deconvolution.
Cord-Autopredict - A whole bunch of different analyses.
     Normalization - Check SWAN/BIMQ/Quantile
     Kobor Reference Only - Use only cord blood as ref.
     Adult/Cord Reference - Use adult to select, then cord to subselect.
     Ignore nRBCs - Remove nRBCs from the profile.
     Tight Probes - Gran vs. all

Cell-Type-Vs-All - Try to predict in a univariate fashion. This is our template for all future analyses.

Current Hypotheses:
    1) Adult probes gives you good info. on each cell type. This is not true for cord. Thus diff. profile for ct.
        [This is probably true. High variance in cord is not helpful to deconvolution.]
    2) We can use similarity of magnitude to adult probes to detect mature probes. This allows us to pick out the best ones
       to use, at least for that cell type. [This is very tough to do. cord probes are already sparse. Adult profile doesnt overlap much]
    3) Normality of population matters if we are "combining" cell types. The closer the other group to "normal", the more effective a probe should be. 
        [This is false. Even non-normal other-cell-type probes seem to do well. The important thing is that it represents the alternative classes well]


Adult reference probes better estimate validation values
========================================================

Folders:
  - CellType Vs. All - Prediction of one cell type at a time. Works well for adults, but not cord.
  - Variance Is The Problem - Attempt to demonstrate that the variance of probes is causing errors.


Would like to establish that because probe variance is higher in cord than adult, reference mean incorrectly specifies
the methylation level for a particular cell type. This leads to inaccurate estimation.

- Demonstrate that of discriminating probes, variance in adult is much lower on average than cord. [Paired probes!]
    => Suffers from multiple testing if we do pre-selection in adult.
    => We can see that adult probes are definitely less variable than cord though.
    => Found that it's mean value of probe methylation on down probes, rather than variance that affects R^2. Bias in platform?

- Is it a few bad samples? Some are very well predicted versus others which are not. Why is this?
    => Results! It seems like columns A and G are terrible. Is it the counts that are bad or the array?
    - Next step is to apply COMBAT on the well column.

- Rerun quality control to identify possible bad samples.
    => No. The samples are fine.

Ideas:
    - Can we consider using LDA where references can be learned as pure topics? Then just figure out the mixture of topics in each validation set.
    - Show that if we really separate out the gran values [.4, .5, .6], our r^2 gets much better.
    - Demonstrate that there is consistency is dispersion of probe values in meaghan/bak data. [Paired Probes]
    - Show that selection criteria systematically picks low variance probes. [Multiple Testing Error]
    - Create a validated set of low dispersion/high dispersion probes. Show that picking low dispersion probes improves accuracy on average.
    - Framework for estimating variance [Bootstrap] of deconvolution estimates based on spread of the actual probe.
    - Can we estimate accuracy of deconvolution based on dispersion of underlying probes?
    - Create an underlying probablistic framework and optimize over it.
    - Is it the variability of measurements at the low end that is screwing with our deconvolution?

Cross Cell Type Signals
=======================
TODOs:
    - Run COMBAT with the row as a covariate. 	
    	=> Nope doesn't help.
	
    - Rerun predictions facetted by sample rows/columns/celltype
        - Do prediction and facet by celltype, colour code by row. Add MAD + R^2
        - Do prediction and facet by row, colour by cell type, add MAD + R^2
        - Do prediction and facet by column, colour by cell type, add MAD + R^2
        - DONE
        
    - Remove Granulocytes from signature and recluster to tease out block structure in rest of sig.
    - Do classification using features given by granulocyte signature. Should tease out mono in adult, but not in cord.
    - Do this on a log scale, and use AUC as a measure.
    - Look at correlations between full signatures (adult vs. cord)    

Differentiation Tree
======================
    - Model signature of undifferentiated cells based on known stem cell markers. This would remove noise before doing the estimation.

