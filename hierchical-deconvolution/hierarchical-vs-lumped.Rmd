---
title: "Hierarchical-vs-Lumped"
author: "Louie Dinh"
date: "November 10, 2016"
output: html_document
---

Hierarchical Deconvolution
================

Purpose: Perform deconvolution at  higher cut in the tree by doing selection on groups of cells. Is this better than just doing selection based on one celltype at a time?

Setup
-----
Do some setup.

Set global options to supress noisy output.
```{r global_options, include=FALSE}
knitr::opts_chunk$set(warning=FALSE, message=FALSE)
```

Initial data load.
```{r, include=FALSE}
## Libraries And Constants ##
library(FlowSorted.Blood.450k)
source("../lib/deconvolution-utils.R")

# The cell types that we're interested in.
ADULT_CELL_TYPES = c("CD8T","CD4T", "NK", "Bcell", "Mono", "Gran")
CELL_TYPES <- c("CD8T","CD4T", "NK", "Bcell", "Mono", "Gran", "nRBC")

## Load the adult data ##
adult_sorted_rgset <- FlowSorted.Blood.450k
adult_mset <- preprocessRaw(adult_sorted_rgset)
# Remove unwanted cell types.
adult_mset <- adult_mset[,adult_mset$CellType %in% ADULT_CELL_TYPES]

# Retrieve betas and our final PD.
adult_betas <- getBeta(adult_mset) %>% na.omit()
colnames(adult_betas) <- sub('+', 'T', colnames(adult_betas), fixed=T)
adult_pd <- pData(adult_mset)
adult_pd$Sample_Name <-  sub('+', 'T', adult_pd$Sample_Name, fixed=T)

## Load the Meaghan's sorted cord data ##
sheet <- read.450k.sheet("/Users/louie/Projects/cord-blood-deconvolution/Data-Raw/sorted")
msorted_rgset <- read.450k.exp(targets=sheet)

pData(msorted_rgset) <- pData(msorted_rgset) %>% select(-(WB_Perc_Neut:MC_Perc_CD8T),
                                                                -Basename,
                                                                -filenames)
pData(msorted_rgset)$CellType <- to_canonical_cell_types(v = msorted_rgset$Tissue,
                        cell_types = c("WB", "CBMC", "CD4T", "CD8T", "G", "Mo", "B", "NK", "RBC"),
                        canon_cell_types = c("WB", "CBMC", "CD4T", "CD8T", "Gran", "Mono", "Bcell", "NK", "nRBC"))
msorted_rgset$Tissue <- NULL

msorted_mset <- preprocessRaw(msorted_rgset)
msorted_mset <- msorted_mset[,msorted_mset$CellType %in% CELL_TYPES]
msorted_betas <- getBeta(msorted_mset)
msorted_pd <- pData(msorted_mset)

## Load Meagan's  validation data. ##
validation_rgset <- read.450k.exp(base = "/Users/louie/Projects/cord-blood-deconvolution/Data-Raw/validation", recursive = TRUE)
# Load in the counts
validation_counts <- read.csv("/Users/louie/Projects/cord-blood-deconvolution/Data/validation-counts.csv", header=T)
validation_counts <- gather(validation_counts, CellType, True.Prop, -SampleName)
validation_counts$True.Prop <- validation_counts$True.Prop / 100
validation_counts$CellType <- to_canonical_cell_types(validation_counts$CellType,
                        c("CD8T", "CD4T", "NK", "Bcell", "Monocytes", "Granulocytes", "nRBCs"),
                        c("CD8T","CD4T","NK","Bcell","Mono", "Gran", "nRBC"))

# Rename the samples in the rgset to match our counts data.
sample2array <- read.csv("/Users/louie/Projects/cord-blood-deconvolution/Data/validation-sample_to_array.csv", header=T)
# Map of array_ids to sample_ids
idx <- match(sampleNames(validation_rgset), sample2array$ArrayName)
sampleNames(validation_rgset) <- sample2array$SampleName[idx]
# Get the Betas
validation_mset <- preprocessRaw(validation_rgset)
validation_betas <- getBeta(validation_mset)
```

Load our new probe selection procedure.
```{r}
source("select_probes_grouped.R")
```

Hierarchical Vs. Lumped
================================

```{r}
N=100
lymphoid_cells <- c("Gran", "Mono")
dm_probes_lymphoid <- select_probes_t_grouped_separation(betas=msorted_betas,
                                           target_cts=lymphoid_cells,
                                           cell_type_ind=msorted_pd$CellType,
                                           cell_types=CELL_TYPES, N=N)

t_cells <- c("CD4T", "CD8T")
dm_probes_t <- select_probes_t_grouped_separation(betas=msorted_betas,
                                           target_cts=t_cells,
                                           cell_type_ind=msorted_pd$CellType,
                                           cell_types=CELL_TYPES, N=N)

dm_probes_b <- select_probes_t_grouped_separation(betas=msorted_betas,
                                           target_cts="Bcell",
                                           cell_type_ind=msorted_pd$CellType,
                                           cell_types=CELL_TYPES, N=N)

dm_probes_nk <- select_probes_t_grouped_separation(betas=msorted_betas,
                                           target_cts="NK",
                                           cell_type_ind=msorted_pd$CellType,
                                           cell_types=CELL_TYPES, N=N)

dm_probes_nrbc <- select_probes_t_grouped_separation(betas=msorted_betas,
                                           target_cts="nRBC",
                                           cell_type_ind=msorted_pd$CellType,
                                           cell_types=CELL_TYPES, N=N)

probes <- c(dm_probes_lymphoid,
            dm_probes_t,
            dm_probes_b,
            dm_probes_nk,
            dm_probes_nrbc)
plot_probes(msorted_betas, probes, msorted_pd$CellType)


# Being careful to keep the probe profiles separate here.
cord_profile <- construct_profiles(betas=msorted_betas, probes=probes, cell_types=CELL_TYPES, 
                   cell_type_ind=msorted_pd$CellType)

est_cell_counts <- minfi:::projectCellType(validation_betas[rownames(cord_profile),],
                                           cord_profile,
                                           nonnegative = T,
                                           lessThanOne = T)
validation_data <- compare_estimation_to_truth(est_cell_counts,
                                               validation_counts)
plot_deconv_accuracy(validation_data)

# Now recombine the counts.
validation_data_lymphoid <- validation_data %>% filter(CellType %in% lymphoid_cells) %>% 
                      group_by(SampleName) %>%
                      summarize(CellType=paste(lymphoid_cells, collapse=""),
                                True.Prop = sum(True.Prop),
                                Est.Prop=sum(Est.Prop))
validation_data_t <- validation_data %>% filter(CellType %in% t_cells) %>% 
                      group_by(SampleName) %>%
                      summarize(CellType=paste(t_cells, collapse=""),
                                True.Prop = sum(True.Prop),
                                Est.Prop=sum(Est.Prop))
validation_data_grouped <- rbind(validation_data %>% 
                                   filter(!(CellType %in% c(lymphoid_cells, t_cells))),
                                 validation_data_lymphoid,
                                 validation_data_t)
plot_deconv_accuracy(validation_data_grouped, plot_scales = "free") + ggtitle("Hierarchical - Separation Based")
```


Sepration Based Deconvolution - One CT at a time. We then lump together the results to check against
grouped selection.
```{r}
N = 100
lymphoid_cells <- c("Gran", "Mono")
t_cells <- c("CD4T", "CD8T")
other_cells <- c("NK", "nRBC", "Bcell")

dm_probes_by_ct <- select_probes_t_separation_by_ct(betas=msorted_betas,
                                                    cell_type_ind=msorted_pd$CellType,
                                                    cell_types=CELL_TYPES, N=N)

probes <- unlist(dm_probes_by_ct)
plot_probes(msorted_betas, probes, msorted_pd$CellType)

# Being careful to keep the probe profiles separate here.
cord_profile <- construct_profiles(betas=msorted_betas, probes=probes, cell_types=CELL_TYPES, 
                   cell_type_ind=msorted_pd$CellType)
est_cell_counts <- minfi:::projectCellType(validation_betas[rownames(cord_profile),],
                                           cord_profile,
                                           nonnegative = T,
                                           lessThanOne = T)
validation_data <- compare_estimation_to_truth(est_cell_counts,
                                               validation_counts)
plot_deconv_accuracy(validation_data, plot_scales="free")

# Now recombine the counts.
validation_data_lymphoid <- validation_data %>% filter(CellType %in% lymphoid_cells) %>% 
                      group_by(SampleName) %>%
                      summarize(CellType=paste(lymphoid_cells, collapse=""),
                                True.Prop = sum(True.Prop),
                                Est.Prop=sum(Est.Prop))
validation_data_t <- validation_data %>% filter(CellType %in% t_cells) %>% 
                      group_by(SampleName) %>%
                      summarize(CellType=paste(t_cells, collapse=""),
                                True.Prop = sum(True.Prop),
                                Est.Prop=sum(Est.Prop))
validation_data_grouped <- rbind(validation_data %>% 
                                   filter(!(CellType %in% c(lymphoid_cells, t_cells))),
                                 validation_data_lymphoid,
                                 validation_data_t)
plot_deconv_accuracy(validation_data_grouped, plot_scales = "free") + ggtitle("Lumped - Separation Based")
```

Conclusion: Roughly the same accuracy improvement when lumping cell types versus selection.