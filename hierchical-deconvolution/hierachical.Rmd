---
title: "Hierachical"
author: "Louie Dinh"
date: "November 2, 2016"
output: html_document
---

Hierarchical Deconvolution
================

Purpose: Compare different probe selection strategies under the hierarchical regime.

Setup
-----
Do some setup.

Set global options to supress noisy output.
```{r global_options, include=FALSE}
knitr::opts_chunk$set(warning=FALSE, message=FALSE)
```

Initial data load.
```{r, include=FALSE}
## Libraries And Constants ##
library(FlowSorted.Blood.450k)
source("../lib/deconvolution-utils.R")

# The cell types that we're interested in.
ADULT_CELL_TYPES = c("CD8T","CD4T", "NK", "Bcell", "Mono", "Gran")
CELL_TYPES <- c("CD8T","CD4T", "NK", "Bcell", "Mono", "Gran", "nRBC")

## Load the adult data ##
adult_sorted_rgset <- FlowSorted.Blood.450k
adult_mset <- preprocessRaw(adult_sorted_rgset)
# Remove unwanted cell types.
adult_mset <- adult_mset[,adult_mset$CellType %in% ADULT_CELL_TYPES]

# Retrieve betas and our final PD.
adult_betas <- getBeta(adult_mset) %>% na.omit()
colnames(adult_betas) <- sub('+', 'T', colnames(adult_betas), fixed=T)
adult_pd <- pData(adult_mset)
adult_pd$Sample_Name <-  sub('+', 'T', adult_pd$Sample_Name, fixed=T)

## Load the Meaghan's sorted cord data ##
sheet <- read.450k.sheet("/Users/louie/Projects/cord-blood-deconvolution/Data-Raw/sorted")
msorted_rgset <- read.450k.exp(targets=sheet)

pData(msorted_rgset) <- pData(msorted_rgset) %>% select(-(WB_Perc_Neut:MC_Perc_CD8T),
                                                                -Basename,
                                                                -filenames)
pData(msorted_rgset)$CellType <- to_canonical_cell_types(v = msorted_rgset$Tissue,
                        cell_types = c("WB", "CBMC", "CD4T", "CD8T", "G", "Mo", "B", "NK", "RBC"),
                        canon_cell_types = c("WB", "CBMC", "CD4T", "CD8T", "Gran", "Mono", "Bcell", "NK", "nRBC"))
msorted_rgset$Tissue <- NULL

msorted_mset <- preprocessRaw(msorted_rgset)
msorted_mset <- msorted_mset[,msorted_mset$CellType %in% CELL_TYPES]
msorted_betas <- getBeta(msorted_mset)
msorted_pd <- pData(msorted_mset)


## Load Bakulski's sorted data. ##
library(FlowSorted.CordBlood.450k)
bsorted <- FlowSorted.CordBlood.450k
pData(bsorted) <- pData(bsorted) %>% select(X, Sex, CellType)
pData(bsorted)$CellType<- to_canonical_cell_types(v = pData(bsorted)$CellType,
                        cell_types = c("WholeBlood", "CD4T", "CD8T", "Gran", "Mono", "Bcell", "NK", "nRBC"),
                        canon_cell_types = c("WB", "CD4T", "CD8T", "Gran", "Mono", "Bcell", "NK", "nRBC"))
bsorted$Array_Name <- as.character(bsorted$X)
bsorted$X <- NULL
bsorted$Sex <- as.character(bsorted$Sex)

# Get the Betas and pd
bsorted_mset <- preprocessRaw(bsorted)
# Remove unwanted cell types.
bsorted_mset <- bsorted_mset[,bsorted_mset$CellType %in% CELL_TYPES]
bsorted_betas <- getBeta(bsorted_mset)
bsorted_pd <- pData(bsorted_mset)
bsorted_pd$SampleName <- bsorted_pd$Array_Name
bsorted_pd$Array_Name <- NULL


## Load Meagan's  validation data. ##
validation_rgset <- read.450k.exp(base = "/Users/louie/Projects/cord-blood-deconvolution/Data-Raw/validation", recursive = TRUE)
# Load in the counts
validation_counts <- read.csv("/Users/louie/Projects/cord-blood-deconvolution/Data/validation-counts.csv", header=T)
validation_counts <- gather(validation_counts, CellType, True.Prop, -SampleName)
validation_counts$True.Prop <- validation_counts$True.Prop / 100
validation_counts$CellType <- to_canonical_cell_types(validation_counts$CellType,
                        c("CD8T", "CD4T", "NK", "Bcell", "Monocytes", "Granulocytes", "nRBCs"),
                        c("CD8T","CD4T","NK","Bcell","Mono", "Gran", "nRBC"))

# Rename the samples in the rgset to match our counts data.
sample2array <- read.csv("/Users/louie/Projects/cord-blood-deconvolution/Data/validation-sample_to_array.csv", header=T)
# Map of array_ids to sample_ids
idx <- match(sampleNames(validation_rgset), sample2array$ArrayName)
sampleNames(validation_rgset) <- sample2array$SampleName[idx]
# Get the Betas
validation_mset <- preprocessRaw(validation_rgset)
validation_betas <- getBeta(validation_mset)
```

Load our new probe selection procedure.
```{r}
source("select_probes_grouped.R")
```

Lymphoid
========

```{r}
lymphoid_cells <- c("Gran")
dm_probes <- select_probes_t_grouped(betas=msorted_betas,
                                           target_cts=lymphoid_cells,
                                           cell_type_ind=msorted_pd$CellType,
                                           cell_types=CELL_TYPES, N=50)
probes <- unlist(dm_probes)
plot_probes(msorted_betas, probes, msorted_pd$CellType)

# Being careful to keep the probe profiles separate here.
cord_profile <- construct_profiles(betas=msorted_betas, probes=probes, cell_types=CELL_TYPES, 
                   cell_type_ind=msorted_pd$CellType)
est_cell_counts <- minfi:::projectCellType(validation_betas[rownames(cord_profile),],
                                           cord_profile,
                                           nonnegative = T,
                                           lessThanOne = T)
validation_data <- compare_estimation_to_truth(est_cell_counts,
                                               validation_counts)

# Now recombine the counts.
validation_data_gm <- validation_data %>% filter(CellType %in% lymphoid_cells) %>% 
                      group_by(SampleName) %>%
                      summarize(CellType=paste(lymphoid_cells, collapse=""),
                                True.Prop = sum(True.Prop),
                                Est.Prop=sum(Est.Prop))
validation_data_grouped <- rbind(validation_data %>% filter(!(CellType %in% lymphoid_cells)),
                                 validation_data_gm)
plot_deconv_accuracy(validation_data_grouped) + ggtitle("Normal Selection")
```

Merged Gran/Mono
```{r}
lymphoid_cells <- c("Gran", "Mono")
dm_probes <- select_probes_t_grouped(betas=msorted_betas,
                                           target_cts=lymphoid_cells,
                                           cell_type_ind=msorted_pd$CellType,
                                           cell_types=CELL_TYPES, N=50)
probes <- unlist(dm_probes)
plot_probes(msorted_betas, probes, msorted_pd$CellType)

# Being careful to keep the probe profiles separate here.
cord_profile <- construct_profiles(betas=msorted_betas, probes=probes, cell_types=CELL_TYPES, 
                   cell_type_ind=msorted_pd$CellType)
est_cell_counts <- minfi:::projectCellType(validation_betas[rownames(cord_profile),],
                                           cord_profile,
                                           nonnegative = T,
                                           lessThanOne = T)
validation_data <- compare_estimation_to_truth(est_cell_counts,
                                               validation_counts)
# Now recombine the counts.
validation_data_gm <- validation_data %>% filter(CellType %in% lymphoid_cells) %>% 
                      group_by(SampleName) %>%
                      summarize(CellType=paste(lymphoid_cells, collapse=""),
                                True.Prop = sum(True.Prop),
                                Est.Prop=sum(Est.Prop))
validation_data_grouped <- rbind(validation_data %>% filter(!(CellType %in% lymphoid_cells)),
                                 validation_data_gm)
plot_deconv_accuracy(validation_data_grouped) + ggtitle("GranMono - All Probes")

## Try only the up probes
probes <- dm_probes$Up
plot_probes(msorted_betas, probes, msorted_pd$CellType)
# Being careful to keep the probe profiles separate here.
cord_profile <- construct_profiles(betas=msorted_betas, probes=probes, cell_types=CELL_TYPES, 
                   cell_type_ind=msorted_pd$CellType)
est_cell_counts <- minfi:::projectCellType(validation_betas[rownames(cord_profile),],
                                           cord_profile,
                                           nonnegative = T,
                                           lessThanOne = T)
validation_data <- compare_estimation_to_truth(est_cell_counts,
                                               validation_counts)
# Now recombine the counts.
validation_data_gm <- validation_data %>% filter(CellType %in% lymphoid_cells) %>% 
                      group_by(SampleName) %>%
                      summarize(CellType=paste(lymphoid_cells, collapse=""),
                                True.Prop = sum(True.Prop),
                                Est.Prop=sum(Est.Prop))
validation_data_grouped <- rbind(validation_data %>% filter(!(CellType %in% lymphoid_cells)),
                                 validation_data_gm)
plot_deconv_accuracy(validation_data_grouped) 

probes <- dm_probes$Down
# Being careful to keep the probe profiles separate here.
cord_profile <- construct_profiles(betas=msorted_betas, probes=probes, cell_types=CELL_TYPES, 
                   cell_type_ind=msorted_pd$CellType)
est_cell_counts <- minfi:::projectCellType(validation_betas[rownames(cord_profile),],
                                           cord_profile,
                                           nonnegative = T,
                                           lessThanOne = T)
validation_data <- compare_estimation_to_truth(est_cell_counts,
                                               validation_counts)
# Being careful to keep the probe profiles separate here.
cord_profile <- construct_profiles(betas=msorted_betas, probes=probes, cell_types=CELL_TYPES, 
                   cell_type_ind=msorted_pd$CellType)
est_cell_counts <- minfi:::projectCellType(validation_betas[rownames(cord_profile),],
                                           cord_profile,
                                           nonnegative = T,
                                           lessThanOne = T)
validation_data <- compare_estimation_to_truth(est_cell_counts,
                                               validation_counts)
# Now recombine the counts.
validation_data_gm <- validation_data %>% filter(CellType %in% lymphoid_cells) %>% 
                      group_by(SampleName) %>%
                      summarize(CellType=paste(lymphoid_cells, collapse=""),
                                True.Prop = sum(True.Prop),
                                Est.Prop=sum(Est.Prop))
validation_data_grouped <- rbind(validation_data %>% filter(!(CellType %in% lymphoid_cells)),
                                 validation_data_gm)
plot_deconv_accuracy(validation_data_grouped) + ggtitle("GranMono - High Probes")
```

Select probes for everyone else as well.
```{r}
lymphoid_cells <- c("Gran", "Mono")
dm_probes_lymphoid <- select_probes_t_grouped(betas=msorted_betas,
                                           target_cts=lymphoid_cells,
                                           cell_type_ind=msorted_pd$CellType,
                                           cell_types=CELL_TYPES, N=50)
plot_probes(msorted_betas, unlist(dm_probes_lymphoid), msorted_pd$CellType)

myeloid_cells <- c("CD4T", "CD8T", "Bcell")
dm_probes_myeloid <- select_probes_t_grouped(betas=msorted_betas,
                                           target_cts=myeloid_cells,
                                           cell_type_ind=msorted_pd$CellType,
                                           cell_types=CELL_TYPES, N=50)
plot_probes(msorted_betas, unlist(dm_probes_myeloid), msorted_pd$CellType)

dm_probes_nk <- select_probes_t_grouped(betas=msorted_betas,
                                           target_cts="NK",
                                           cell_type_ind=msorted_pd$CellType,
                                           cell_types=CELL_TYPES, N=50)
plot_probes(msorted_betas, unlist(dm_probes_nk), msorted_pd$CellType)

dm_probes_nrbc <- select_probes_t_grouped(betas=msorted_betas,
                                           target_cts="nRBC",
                                           cell_type_ind=msorted_pd$CellType,
                                           cell_types=CELL_TYPES, N=50)
plot_probes(msorted_betas, unlist(dm_probes_nrbc), msorted_pd$CellType)

probes <- c(unlist(dm_probes_lymphoid),
            unlist(dm_probes_myeloid),
            unlist(dm_probes_nk),
            unlist(dm_probes_nrbc))

# Being careful to keep the probe profiles separate here.
cord_profile <- construct_profiles(betas=msorted_betas, probes=probes, cell_types=CELL_TYPES, 
                   cell_type_ind=msorted_pd$CellType)
est_cell_counts <- minfi:::projectCellType(validation_betas[rownames(cord_profile),],
                                           cord_profile,
                                           nonnegative = T,
                                           lessThanOne = T)
validation_data <- compare_estimation_to_truth(est_cell_counts,
                                               validation_counts)

# Now recombine the counts.
validation_data_lymphoid <- validation_data %>% filter(CellType %in% lymphoid_cells) %>% 
                      group_by(SampleName) %>%
                      summarize(CellType=paste(lymphoid_cells, collapse=""),
                                True.Prop = sum(True.Prop),
                                Est.Prop=sum(Est.Prop))
validation_data_myeloid <- validation_data %>% filter(CellType %in% myeloid_cells) %>% 
                      group_by(SampleName) %>%
                      summarize(CellType=paste(myeloid_cells, collapse=""),
                                True.Prop = sum(True.Prop),
                                Est.Prop=sum(Est.Prop))
validation_data_grouped <- rbind(validation_data %>% 
                                   filter(!(CellType %in% c(lymphoid_cells, myeloid_cells))),
                                 validation_data_lymphoid,
                                 validation_data_myeloid)
plot_deconv_accuracy(validation_data_grouped, plot_scales = "free") + ggtitle("GranMono, CD4CD8B,NK,nRBC")
```

Stepwise, GranMono First
==========================

```{r}
lymphoid_cells <- c("Gran", "Mono")
dm_probes_lymphoid <- select_probes_t_grouped(betas=msorted_betas,
                                           target_cts=lymphoid_cells,
                                           cell_type_ind=msorted_pd$CellType,
                                           cell_types=CELL_TYPES, N=50)

# Regress out Gran/Mono first because of imbalanced errors.
probes <- dm_probes_lymphoid$Down
# Being careful to keep the probe profiles separate here.
cord_profile <- construct_profiles(betas=msorted_betas, probes=probes, cell_types=CELL_TYPES, 
                   cell_type_ind=msorted_pd$CellType)
est_cell_counts <- minfi:::projectCellType(validation_betas[rownames(cord_profile),],
                                           cord_profile,
                                           nonnegative = T,
                                           lessThanOne = T)
validation_data <- compare_estimation_to_truth(est_cell_counts,
                                               validation_counts)
validation_data_lymphoid <- validation_data %>% filter(CellType %in% lymphoid_cells) %>% 
                      group_by(SampleName) %>%
                      summarize(CellType=paste(lymphoid_cells, collapse=""),
                                True.Prop = sum(True.Prop),
                                Est.Prop=sum(Est.Prop))
validation_data_rest <- validation_data %>% filter(!(CellType %in% lymphoid_cells))
validation_data_grouped <- rbind(validation_data_lymphoid, validation_data_rest)
plot_deconv_accuracy(validation_data_grouped)

gran_props <- validation_data %>%
                    filter(CellType == "Gran") %>%
                    select(SampleName, Est.Prop)
gran_names <- gran_props$SampleName
temp <- as.numeric(gran_props$Est.Prop)
names(temp) <- gran_names
gran_props <- temp

mono_props <- validation_data %>%
                    filter(CellType == "Mono") %>%
                    select(SampleName, Est.Prop)
mono_names <- mono_props$SampleName
temp <- as.numeric(mono_props$Est.Prop)
names(temp) <- mono_names
mono_props <- temp

# Build rest of profile, and regress out signal of gran/mono before deconvolution. 
myeloid_cells <- c("CD4T", "CD8T", "Bcell")
dm_probes_myeloid <- select_probes_t_grouped(betas=msorted_betas,
                                           target_cts=myeloid_cells,
                                           cell_type_ind=msorted_pd$CellType,
                                           cell_types=CELL_TYPES, N=50)
dm_probes_nk <- select_probes_t_grouped(betas=msorted_betas,
                                           target_cts="NK",
                                           cell_type_ind=msorted_pd$CellType,
                                           cell_types=CELL_TYPES, N=50)
dm_probes_nrbc <- select_probes_t_grouped(betas=msorted_betas,
                                           target_cts="nRBC",
                                           cell_type_ind=msorted_pd$CellType,
                                           cell_types=CELL_TYPES, N=50)
probes <- c(unlist(dm_probes_myeloid),
            unlist(dm_probes_nk),
            unlist(dm_probes_nrbc))
cord_profile <- construct_profiles(betas=msorted_betas, probes=probes, cell_types=CELL_TYPES, 
                   cell_type_ind=msorted_pd$CellType)
validation_betas_subset <- validation_betas[row.names(cord_profile),]

gran_betas <- cord_profile[,"Gran"] %*% t(gran_props[colnames(validation_betas_subset)])
mono_betas <- cord_profile[,"Mono"] %*% t(mono_props[colnames(validation_betas_subset)])
validation_betas_no_lymphoid <- validation_betas_subset - gran_betas - mono_betas

est_cell_counts <- minfi:::projectCellType(validation_betas_no_lymphoid,
                                           cord_profile,
                                           nonnegative = T,
                                           lessThanOne = T)
# Nuke estimates for lymphoid cells
est_cell_counts <- est_cell_counts[,!(colnames(est_cell_counts) %in% lymphoid_cells)]
validation_data_rest <- compare_estimation_to_truth(est_cell_counts, validation_counts)
validation_data_myeloid <- validation_data_rest %>% filter(CellType %in% myeloid_cells) %>% 
                      group_by(SampleName) %>%
                      summarize(CellType=paste(myeloid_cells, collapse=""),
                                True.Prop = sum(True.Prop),
                                Est.Prop=sum(Est.Prop))
validation_data_grouped <- rbind(validation_data_rest %>% 
                                   filter(!(CellType %in% c(lymphoid_cells, myeloid_cells))),
                                 validation_data_lymphoid,
                                 validation_data_myeloid)

plot_deconv_accuracy(validation_data_grouped, plot_scales = "free") + ggtitle("Stepwise GranMono Then Rest")
```

For other probes, Gran are less methylated than they should be.


Use Only Low Probes
===================
```{r}
lymphoid_cells <- c("Gran", "Mono")
dm_probes_lymphoid <- select_probes_t_grouped(betas=msorted_betas,
                                           target_cts=lymphoid_cells,
                                           cell_type_ind=msorted_pd$CellType,
                                           cell_types=CELL_TYPES, N=50)

myeloid_cells <- c("CD4T", "CD8T", "Bcell")
dm_probes_myeloid <- select_probes_t_grouped(betas=msorted_betas,
                                           target_cts=myeloid_cells,
                                           cell_type_ind=msorted_pd$CellType,
                                           cell_types=CELL_TYPES, N=50)

dm_probes_nk <- select_probes_t_grouped(betas=msorted_betas,
                                           target_cts="NK",
                                           cell_type_ind=msorted_pd$CellType,
                                           cell_types=CELL_TYPES, N=50)

dm_probes_nrbc <- select_probes_t_grouped(betas=msorted_betas,
                                           target_cts="nRBC",
                                           cell_type_ind=msorted_pd$CellType,
                                           cell_types=CELL_TYPES, N=50)

probes <- c(dm_probes_lymphoid$Up,
            dm_probes_myeloid$Up,
            dm_probes_nk$Up,
            dm_probes_nrbc$Up)
plot_probes(msorted_betas, probes, msorted_pd$CellType)

# Being careful to keep the probe profiles separate here.
cord_profile <- construct_profiles(betas=msorted_betas, probes=probes, cell_types=CELL_TYPES, 
                   cell_type_ind=msorted_pd$CellType)
est_cell_counts <- minfi:::projectCellType(validation_betas[rownames(cord_profile),],
                                           cord_profile,
                                           nonnegative = T,
                                           lessThanOne = T)
validation_data <- compare_estimation_to_truth(est_cell_counts,
                                               validation_counts)

# Now recombine the counts.
validation_data_lymphoid <- validation_data %>% filter(CellType %in% lymphoid_cells) %>% 
                      group_by(SampleName) %>%
                      summarize(CellType=paste(lymphoid_cells, collapse=""),
                                True.Prop = sum(True.Prop),
                                Est.Prop=sum(Est.Prop))
validation_data_myeloid <- validation_data %>% filter(CellType %in% myeloid_cells) %>% 
                      group_by(SampleName) %>%
                      summarize(CellType=paste(myeloid_cells, collapse=""),
                                True.Prop = sum(True.Prop),
                                Est.Prop=sum(Est.Prop))
validation_data_grouped <- rbind(validation_data %>% 
                                   filter(!(CellType %in% c(lymphoid_cells, myeloid_cells))),
                                 validation_data_lymphoid,
                                 validation_data_myeloid)
plot_deconv_accuracy(validation_data_grouped, plot_scales = "free") + ggtitle("GranMono, CD4CD8, NK, nRBC + Low Only")
```


Use Only Separation Probes
==========================
```{r}
lymphoid_cells <- c("Gran", "Mono")
dm_probes_lymphoid <- select_probes_t_grouped_separation(betas=msorted_betas,
                                           target_cts=lymphoid_cells,
                                           cell_type_ind=msorted_pd$CellType,
                                           cell_types=CELL_TYPES, N=50)

myeloid_cells <- c("CD4T", "CD8T", "Bcell")
dm_probes_myeloid <- select_probes_t_grouped_separation(betas=msorted_betas,
                                           target_cts=myeloid_cells,
                                           cell_type_ind=msorted_pd$CellType,
                                           cell_types=CELL_TYPES, N=50)

dm_probes_nk <- select_probes_t_grouped_separation(betas=msorted_betas,
                                           target_cts="NK",
                                           cell_type_ind=msorted_pd$CellType,
                                           cell_types=CELL_TYPES, N=50)

dm_probes_nrbc <- select_probes_t_grouped_separation(betas=msorted_betas,
                                           target_cts="nRBC",
                                           cell_type_ind=msorted_pd$CellType,
                                           cell_types=CELL_TYPES, N=50)

probes <- c(dm_probes_lymphoid,
            dm_probes_myeloid,
            dm_probes_nk,
            dm_probes_nrbc)
plot_probes(msorted_betas, probes, msorted_pd$CellType)


# Being careful to keep the probe profiles separate here.
cord_profile <- construct_profiles(betas=msorted_betas, probes=probes, cell_types=CELL_TYPES, 
                   cell_type_ind=msorted_pd$CellType)
est_cell_counts <- minfi:::projectCellType(validation_betas[rownames(cord_profile),],
                                           cord_profile,
                                           nonnegative = T,
                                           lessThanOne = T)
validation_data <- compare_estimation_to_truth(est_cell_counts,
                                               validation_counts)
plot_deconv_accuracy(validation_data)

# Now recombine the counts.
validation_data_lymphoid <- validation_data %>% filter(CellType %in% lymphoid_cells) %>% 
                      group_by(SampleName) %>%
                      summarize(CellType=paste(lymphoid_cells, collapse=""),
                                True.Prop = sum(True.Prop),
                                Est.Prop=sum(Est.Prop))
validation_data_myeloid <- validation_data %>% filter(CellType %in% myeloid_cells) %>% 
                      group_by(SampleName) %>%
                      summarize(CellType=paste(myeloid_cells, collapse=""),
                                True.Prop = sum(True.Prop),
                                Est.Prop=sum(Est.Prop))
validation_data_grouped <- rbind(validation_data %>% 
                                   filter(!(CellType %in% c(lymphoid_cells, myeloid_cells))),
                                 validation_data_lymphoid,
                                 validation_data_myeloid)
plot_deconv_accuracy(validation_data_grouped, plot_scales = "free") + ggtitle("Separation Based")
```

