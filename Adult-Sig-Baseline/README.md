Overview
=========

Baseline measure for accuracy of deconvolution. We use the standard methylation deconvolution pipeline of minfi
on the cord blood data (Meaghan), to measure the degradation in performance.

Files
=====
sample_to_array.csv - Mapping of sample_id to array_id. Generated from sorted-samplesheet.csv by joining the sentrix_id and sentrix_position fields.
wb-and-cbmc-counts.csv - Taken from scaled_counts2.csv, switched Ts with TS in the sample names (inconsistency between fractionataed cell types and WB/CBMC sample names)
validation-counts.csv - Updated the header a bit to make it easier to read into R.

