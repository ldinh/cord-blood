> summary(lm(Gran ~ 0 + PC1 + PC2 + PC3 + PC4 + PC5, data = (data %>% filter(SampleType == "WB"))))

Call:
lm(formula = Gran ~ 0 + PC1 + PC2 + PC3 + PC4 + PC5, data = (data %>% 
    filter(SampleType == "WB")))

Residuals:
        1         2         3         4         5         6 
 0.161356  0.044506 -0.110371 -0.004747 -0.057493  0.059890 

Coefficients:
      Estimate Std. Error t value Pr(>|t|)
PC1 -0.0008031  0.0151492  -0.053    0.966
PC2  0.0303862  0.0063348   4.797    0.131
PC3 -0.0130813  0.0107844  -1.213    0.439
PC4 -0.1651566  0.0793870  -2.080    0.285
PC5  0.0223344  0.0593856   0.376    0.771

Residual standard error: 0.2171 on 1 degrees of freedom
Multiple R-squared:  0.9741,    Adjusted R-squared:  0.8445 
F-statistic: 7.516 on 5 and 1 DF,  p-value: 0.2698

> summary(lm(CD4T~ 0 + PC1 + PC2 + PC3 + PC4 + PC5, data = (data %>% filter(SampleType == "WB"))))

Call:
lm(formula = CD4T ~ 0 + PC1 + PC2 + PC3 + PC4 + PC5, data = (data %>% 
    filter(SampleType == "WB")))

Residuals:
        1         2         3         4         5         6 
 0.068638  0.018932 -0.046950 -0.002019 -0.024456  0.025476 

Coefficients:
     Estimate Std. Error t value Pr(>|t|)
PC1 -0.006808   0.006444  -1.056    0.483
PC2  0.011590   0.002695   4.301    0.145
PC3 -0.006045   0.004588  -1.318    0.413
PC4 -0.055400   0.033770  -1.641    0.348
PC5  0.021643   0.025262   0.857    0.549

Residual standard error: 0.09233 on 1 degrees of freedom
Multiple R-squared:  0.952, Adjusted R-squared:  0.7123 
F-statistic: 3.971 on 5 and 1 DF,  p-value: 0.3629

> summary(lm(CD8T ~ 0 + PC1 + PC2 + PC3 + PC4 + PC5, data = (data %>% filter(SampleType == "WB"))))

Call:
lm(formula = CD8T ~ 0 + PC1 + PC2 + PC3 + PC4 + PC5, data = (data %>% 
    filter(SampleType == "WB")))

Residuals:
        1         2         3         4         5         6 
 0.034755  0.009586 -0.023773 -0.001022 -0.012384  0.012900 

Coefficients:
      Estimate Std. Error t value Pr(>|t|)
PC1 -0.0041847  0.0032631  -1.282    0.422
PC2  0.0067088  0.0013645   4.917    0.128
PC3 -0.0003325  0.0023229  -0.143    0.909
PC4 -0.0305686  0.0170995  -1.788    0.325
PC5  0.0050612  0.0127914   0.396    0.760

Residual standard error: 0.04675 on 1 degrees of freedom
Multiple R-squared:  0.9621,    Adjusted R-squared:  0.7725 
F-statistic: 5.074 on 5 and 1 DF,  p-value: 0.3244

> summary(lm(Bcell ~ 0 + PC1 + PC2 + PC3 + PC4 + PC5, data = (data %>% filter(SampleType == "WB"))))

Call:
lm(formula = Bcell ~ 0 + PC1 + PC2 + PC3 + PC4 + PC5, data = (data %>% 
    filter(SampleType == "WB")))

Residuals:
         1          2          3          4          5          6 
 0.0286847  0.0079119 -0.0196210 -0.0008439 -0.0102207  0.0106469 

Coefficients:
      Estimate Std. Error t value Pr(>|t|)
PC1 -0.0020847  0.0026931  -0.774    0.581
PC2  0.0030430  0.0011262   2.702    0.226
PC3 -0.0007430  0.0019172  -0.388    0.765
PC4 -0.0180577  0.0141129  -1.280    0.422
PC5 -0.0007397  0.0105572  -0.070    0.955

Residual standard error: 0.03859 on 1 degrees of freedom
Multiple R-squared:  0.8925,    Adjusted R-squared:  0.3553 
F-statistic: 1.661 on 5 and 1 DF,  p-value: 0.5271

> summary(lm(Mono ~ 0 + PC1 + PC2 + PC3 + PC4 + PC5, data = (data %>% filter(SampleType == "WB"))))

Call:
lm(formula = Mono ~ 0 + PC1 + PC2 + PC3 + PC4 + PC5, data = (data %>% 
    filter(SampleType == "WB")))

Residuals:
         1          2          3          4          5          6 
 0.0185767  0.0051239 -0.0127069 -0.0005465 -0.0066191  0.0068951 

Coefficients:
      Estimate Std. Error t value Pr(>|t|)
PC1 -0.0007554  0.0017441  -0.433    0.740
PC2  0.0040000  0.0007293   5.485    0.115
PC3 -0.0002082  0.0012416  -0.168    0.894
PC4 -0.0178619  0.0091397  -1.954    0.301
PC5  0.0036794  0.0068370   0.538    0.686

Residual standard error: 0.02499 on 1 degrees of freedom
Multiple R-squared:  0.9742,    Adjusted R-squared:  0.8453 
F-statistic: 7.556 on 5 and 1 DF,  p-value: 0.2691

> summary(lm(NK ~ 0 + PC1 + PC2 + PC3 + PC4 + PC5, data = (data %>% filter(SampleType == "WB"))))

Call:
lm(formula = NK ~ 0 + PC1 + PC2 + PC3 + PC4 + PC5, data = (data %>% 
    filter(SampleType == "WB")))

Residuals:
         1          2          3          4          5          6 
 6.030e-04  1.663e-04 -4.125e-04 -1.774e-05 -2.149e-04  2.238e-04 

Coefficients:
      Estimate Std. Error t value Pr(>|t|)   
PC1 -1.396e-03  5.661e-05 -24.663  0.02580 * 
PC2  2.227e-03  2.367e-05  94.091  0.00677 **
PC3 -2.935e-04  4.030e-05  -7.284  0.08686 . 
PC4 -1.366e-02  2.967e-04 -46.041  0.01383 * 
PC5 -8.844e-04  2.219e-04  -3.985  0.15651   
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 0.0008111 on 1 degrees of freedom
Multiple R-squared:  0.9999,    Adjusted R-squared:  0.9994 
F-statistic:  2126 on 5 and 1 DF,  p-value: 0.01646

> 
> summary(lm(Gran ~ 0 + PC1 + PC2 + PC3 + PC4 + PC5, data = (data %>% filter(SampleType == "WBC"))))

Call:
lm(formula = Gran ~ 0 + PC1 + PC2 + PC3 + PC4 + PC5, data = (data %>% 
    filter(SampleType == "WBC")))

Residuals:
      Min        1Q    Median        3Q       Max 
-0.039434 -0.006453  0.006678  0.013223  0.017676 

Coefficients:
      Estimate Std. Error t value Pr(>|t|)    
PC1  0.0075496  0.0006456  11.693 7.56e-06 ***
PC2 -0.0703925  0.0022094 -31.860 7.76e-09 ***
PC3 -0.1139572  0.0134501  -8.473 6.31e-05 ***
PC4  0.1620146  0.0138179  11.725 7.43e-06 ***
PC5 -0.0270141  0.0063119  -4.280  0.00366 ** 
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 0.02204 on 7 degrees of freedom
Multiple R-squared:  0.9989,    Adjusted R-squared:  0.9981 
F-statistic:  1264 on 5 and 7 DF,  p-value: 3.496e-10

> summary(lm(CD4T ~ 0 + PC1 + PC2 + PC3 + PC4 + PC5, data = (data %>% filter(SampleType == "WBC"))))

Call:
lm(formula = CD4T ~ 0 + PC1 + PC2 + PC3 + PC4 + PC5, data = (data %>% 
    filter(SampleType == "WBC")))

Residuals:
      Min        1Q    Median        3Q       Max 
-0.056152 -0.032845  0.004254  0.024255  0.048375 

Coefficients:
     Estimate Std. Error t value Pr(>|t|)   
PC1 -0.002047   0.001258  -1.627  0.14783   
PC2 -0.016491   0.004305  -3.830  0.00645 **
PC3 -0.015625   0.026209  -0.596  0.56983   
PC4  0.059135   0.026926   2.196  0.06409 . 
PC5 -0.001205   0.012299  -0.098  0.92471   
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 0.04294 on 7 degrees of freedom
Multiple R-squared:  0.9363,    Adjusted R-squared:  0.8908 
F-statistic: 20.58 on 5 and 7 DF,  p-value: 0.0004692

> summary(lm(CD8T ~ 0 + PC1 + PC2 + PC3 + PC4 + PC5, data = (data %>% filter(SampleType == "WBC"))))

Call:
lm(formula = CD8T ~ 0 + PC1 + PC2 + PC3 + PC4 + PC5, data = (data %>% 
    filter(SampleType == "WBC")))

Residuals:
     Min       1Q   Median       3Q      Max 
-0.04231 -0.02808 -0.01039  0.02058  0.07238 

Coefficients:
     Estimate Std. Error t value Pr(>|t|)   
PC1 -0.004690   0.001435  -3.269   0.0137 * 
PC2 -0.020656   0.004910  -4.207   0.0040 **
PC3 -0.094271   0.029888  -3.154   0.0161 * 
PC4  0.078137   0.030706   2.545   0.0384 * 
PC5 -0.008964   0.014026  -0.639   0.5431   
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 0.04897 on 7 degrees of freedom
Multiple R-squared:  0.9483,    Adjusted R-squared:  0.9114 
F-statistic: 25.68 on 5 and 7 DF,  p-value: 0.0002291

> summary(lm(Bcell ~ 0 + PC1 + PC2 + PC3 + PC4 + PC5, data = (data %>% filter(SampleType == "WBC"))))

Call:
lm(formula = Bcell ~ 0 + PC1 + PC2 + PC3 + PC4 + PC5, data = (data %>% 
    filter(SampleType == "WBC")))

Residuals:
      Min        1Q    Median        3Q       Max 
-0.015478 -0.003717 -0.002704  0.006953  0.013333 

Coefficients:
      Estimate Std. Error t value Pr(>|t|)    
PC1 -0.0035498  0.0003099 -11.454 8.68e-06 ***
PC2 -0.0141123  0.0010605 -13.307 3.17e-06 ***
PC3  0.0243473  0.0064561   3.771  0.00697 ** 
PC4 -0.0225369  0.0066326  -3.398  0.01148 *  
PC5  0.0255140  0.0030297   8.421 6.56e-05 ***
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 0.01058 on 7 degrees of freedom
Multiple R-squared:  0.9958,    Adjusted R-squared:  0.9928 
F-statistic: 332.5 on 5 and 7 DF,  p-value: 3.684e-08

> summary(lm(Mono ~ 0 + PC1 + PC2 + PC3 + PC4 + PC5, data = (data %>% filter(SampleType == "WBC"))))

Call:
lm(formula = Mono ~ 0 + PC1 + PC2 + PC3 + PC4 + PC5, data = (data %>% 
    filter(SampleType == "WBC")))

Residuals:
      Min        1Q    Median        3Q       Max 
-0.042729 -0.015535  0.003541  0.025017  0.030477 

Coefficients:
      Estimate Std. Error t value Pr(>|t|)    
PC1 -0.0021188  0.0009101  -2.328 0.052761 .  
PC2 -0.0243170  0.0031145  -7.808 0.000106 ***
PC3 -0.0680099  0.0189595  -3.587 0.008891 ** 
PC4  0.0064763  0.0194780   0.332 0.749254    
PC5  0.0419474  0.0088974   4.715 0.002171 ** 
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 0.03106 on 7 degrees of freedom
Multiple R-squared:  0.9737,    Adjusted R-squared:  0.955 
F-statistic: 51.87 on 5 and 7 DF,  p-value: 2.213e-05

> summary(lm(NK ~ 0 + PC1 + PC2 + PC3 + PC4 + PC5, data = (data %>% filter(SampleType == "WBC"))))

Call:
lm(formula = NK ~ 0 + PC1 + PC2 + PC3 + PC4 + PC5, data = (data %>% 
    filter(SampleType == "WBC")))

Residuals:
      Min        1Q    Median        3Q       Max 
-0.040233 -0.015703  0.003787  0.016194  0.032976 

Coefficients:
      Estimate Std. Error t value Pr(>|t|)   
PC1 -0.0040722  0.0008455  -4.816  0.00193 **
PC2 -0.0116901  0.0028934  -4.040  0.00493 **
PC3 -0.0044267  0.0176138  -0.251  0.80878   
PC4  0.0231304  0.0180955   1.278  0.24191   
PC5  0.0019133  0.0082659   0.231  0.82357   
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 0.02886 on 7 degrees of freedom
Multiple R-squared:  0.9613,    Adjusted R-squared:  0.9336 
F-statistic: 34.77 on 5 and 7 DF,  p-value: 8.456e-05

