---
title: "Adult-Deconvolution"
author: "Louie Dinh"
date: "May 31, 2016"
output: html_document
---

This is based on Cord-AutoPredict/adult-deconvolution

```{r}
library(tidyr)
library(dplyr)
library(ggplot2)

library(minfi)
library(GEOquery)
library(genefilter)

source("../lib/deconvolution-utils.R")
```

Retrieve Koestler's data from GEO and extract the validation cell counts.
```{r, eval=FALSE}
### Here we retrieve Koestler's data to extract the cell counts.
# koestler_validation_data <- getGEO("GSE77797", GSEMatrix = TRUE)[[1]]
# save(koestler_validation_data, file="koestler-validation-data.RData")
#
# Optionally use this
# load("koestler-validation-data.RData")
###

# Extract the counts
pd <- data.frame(GSM=sampleNames(koestler_validation_data))
pheno <- pData(koestler_validation_data)
pd$CD4T <- as.numeric(sub("cd4+ t cell (%): ", "", pheno$characteristics_ch1, fixed=T)) / 100
pd$CD8T <- as.numeric(sub("cd8+ t cell (%): ", "", pheno$characteristics_ch1.1, fixed=T)) / 100
pd$Bcell <- as.numeric(sub("b cell (%): ", "", pheno$characteristics_ch1.2, fixed=T)) / 100
pd$NK <- as.numeric(sub("natural killer cell (%): ", "", pheno$characteristics_ch1.3, fixed=T)) / 100
pd$Mono <- as.numeric(sub("monocyte (%): ", "", pheno$characteristics_ch1.4, fixed=T)) / 100
pd$Gran <- as.numeric(sub("granulocyte (%): ", "", pheno$characteristics_ch1.5, fixed=T)) / 100
pd$ArrayName <- pheno$description
pd$SampleType <- sub('.*\\((.*)\\).*', '\\1', pheno$description.1)

pd$SampleName <- paste(pd$GSM, sub("X", "", pd$ArrayName), sep="_")
pd$ArrayName <- NULL
pd$GSM <- NULL
row.names(pd) <- pd$SampleName
## Start doing deconvolution using the standard sites
write.table(pd, file="koestler-validation-data-counts.csv", sep=",", row.names = F)
```

Do deconvolution on the validation set.
```{r}
pd <- read.table("koestler-validation-data-counts.csv", sep=",", header = T)

true_cell_counts <- gather(pd, CellType, True.Prop, -SampleName, -SampleType)

BASEDIR <- "/Users/louie/Projects/cord-blood-deconvolution/"
DATADIR <- paste(BASEDIR, "Data-External/koestler-validation-data", sep="/")
koestler_rgset <- read.450k.exp(base = DATADIR, recursive = TRUE)
# Attach some metadata to the RGSet
row.names(pd) <- pd$SampleName
adf <- as(pd, "AnnotatedDataFrame")
# This is surprisingly important. The dimLabels have to match the reference data!
dimLabels(adf) <- c("sampleNames", "sampleColumns")
phenoData(koestler_rgset) <- adf

res <- data.frame(estimateCellCounts(koestler_rgset))
res$SampleName <- row.names(res)
est_cell_counts <- gather(res, CellType, Est.Prop, -SampleName)

invitro_data <- inner_join(est_cell_counts,
                           true_cell_counts %>% filter(SampleType == "WBC") %>% select(-SampleType),
                           by=c("SampleName", "CellType")) 
plot_deconv_accuracy(invitro_data, plot_scales="free_x") + ggtitle("Adult - Invitro")

invivo_data <- inner_join(est_cell_counts,
                           true_cell_counts %>% filter(SampleType == "WB") %>% select(-SampleType),
                           by=c("SampleName", "CellType")) 
plot_deconv_accuracy(invivo_data, plot_scales="free_x") + ggtitle("Adult - Invivo")
```
