---
title: "Compare-Signatures-Clean-Unclean"
author: "Louie Dinh"
date: '2017-02-01'
output: html_document
---


Initial data load.
```{r, include=FALSE}
## Libraries And Constants ##
library(FlowSorted.Blood.450k)
library(VennDiagram)
source("../lib/deconvolution-utils.R")

# The cell types that we're interested in.
CELL_TYPES <- c("CD8T","CD4T", "NK", "Bcell", "Mono", "Gran", "nRBC")

## Load the Meaghan's sorted cord data ##
sheet <- read.450k.sheet("/Users/louie/Projects/cord-blood-deconvolution/Data-Raw/sorted")
msorted_rgset <- read.450k.exp(targets=sheet)

pData(msorted_rgset) <- pData(msorted_rgset) %>% select(-(WB_Perc_Neut:MC_Perc_CD8T),
                                                                -Basename,
                                                                -filenames)
pData(msorted_rgset)$CellType <- to_canonical_cell_types(v = msorted_rgset$Tissue,
                        cell_types = c("WB", "CBMC", "CD4T", "CD8T", "G", "Mo", "B", "NK", "RBC"),
                        canon_cell_types = c("WB", "CBMC", "CD4T", "CD8T", "Gran", "Mono", "Bcell", "NK", "nRBC"))
msorted_rgset$Tissue <- NULL

msorted_mset <- preprocessRaw(msorted_rgset)
# Get the WB samples.
msorted_wb_mset <- msorted_mset[,msorted_mset$CellType == "WB"]
msorted_wb_pd <- pData(msorted_wb_mset)
msorted_wb_betas <- getBeta(msorted_wb_mset)
colnames(msorted_wb_betas) <- paste("TS", msorted_wb_pd$Participant, sep="")
msorted_wb_counts <- read.csv("/Users/louie/Projects/cord-blood-deconvolution/Data/sorted-counts.csv",
                              header=T)
msorted_wb_counts <- msorted_wb_counts %>%
                      mutate(Gran=Neutro+Eo+Baso) %>%
                      select(-Neutro, -Baso, -Eo)
msorted_wb_counts <- gather(msorted_wb_counts, CellType, True.Prop, -SampleName)
msorted_wb_counts$True.Prop <- msorted_wb_counts$True.Prop / 100
# Get the sorted samples.
msorted_mset <- msorted_mset[,msorted_mset$CellType %in% CELL_TYPES]
msorted_betas <- getBeta(msorted_mset)
msorted_pd <- pData(msorted_mset)

## Load Meagan's validation data. ##
validation_sheet <- read.450k.sheet("/Users/louie/Projects/cord-blood-deconvolution/Data-Raw/validation/")
validation_rgset <- read.450k.exp(targets=validation_sheet)
# Load in the counts
validation_counts <- read.csv("/Users/louie/Projects/cord-blood-deconvolution/Data/validation-counts.csv", header=T)
validation_counts <- gather(validation_counts, CellType, True.Prop, -SampleName)
validation_counts$True.Prop <- validation_counts$True.Prop / 100
validation_counts$CellType <- to_canonical_cell_types(validation_counts$CellType,
                        c("CD8T", "CD4T", "NK", "Bcell", "Monocytes", "Granulocytes", "nRBCs"),
                        c("CD8T","CD4T","NK","Bcell","Mono", "Gran", "nRBC"))

validation_cbcs_raw <- read.csv("/Users/louie/Projects/cord-blood-deconvolution/Data/raw-validation-cbcs.csv", header=T)
validation_cbcs <- gather(validation_cbcs_raw, CellType, Count, -SampleName)
total_cell_counts <- validation_cbcs %>% group_by(SampleName) %>% summarize(TotalCount=sum(Count))
validation_cbcs <- inner_join(validation_cbcs, total_cell_counts, by="SampleName") %>%
  mutate(True.Prop=Count/TotalCount) %>% select(SampleName, CellType, True.Prop)
validation_gran <- validation_cbcs %>% filter(CellType %in% c("Neut", "Eos", "Baso")) %>% 
  group_by(SampleName) %>% summarize(CellType="Gran", True.Prop=sum(True.Prop))
validation_other <- validation_cbcs %>% filter(!(CellType %in% c("Neut", "Eos", "Baso")))
validation_cbcs <- rbind(validation_gran, validation_other)

validation_cbcs_norbc <- gather(validation_cbcs_raw, CellType, Count, -SampleName) %>% filter(CellType != "nRBC")
total_cell_counts <- validation_cbcs_norbc %>% group_by(SampleName) %>% summarize(TotalCount=sum(Count))
validation_cbcs_norbc <- inner_join(validation_cbcs_norbc, total_cell_counts, by="SampleName") %>%
  mutate(True.Prop=Count/TotalCount) %>% select(SampleName, CellType, True.Prop)
validation_gran <- validation_cbcs_norbc %>% filter(CellType %in% c("Neut", "Eos", "Baso")) %>% 
  group_by(SampleName) %>% summarize(CellType="Gran", True.Prop=sum(True.Prop))
validation_other <- validation_cbcs_norbc %>% filter(!(CellType %in% c("Neut", "Eos", "Baso")))
validation_cbcs_norbc <- rbind(validation_gran, validation_other)

# Rename the samples in the rgset to match our counts data.
sample2array <- read.csv("/Users/louie/Projects/cord-blood-deconvolution/Data/validation-sample_to_array.csv", header=T)
# Map of array_ids to sample_ids
idx <- match(sampleNames(validation_rgset), sample2array$ArrayName)
sampleNames(validation_rgset) <- sample2array$SampleName[idx]
# Get the Betas
validation_mset <- preprocessRaw(validation_rgset)
validation_betas <- getBeta(validation_mset)
validation_pd <- pData(validation_mset)
```

```{r}
# Load the cleaned up data.
msorted_betas.combat <- read.table("../Data/sorted_betas_filter_noob_combat.csv", sep=",") %>% as.matrix()
msorted_pd.combat <- read.table("../Data/sorted_pd_filter_noob_combat.csv", sep=",")
validation_betas.combat <- read.table("../Data/validation_betas_filter_noob_combat.csv",
                                      sep=",") %>% as.matrix()
validation_pd.combat <- read.table("../Data/validation_pd_filter_noob_combat.csv",
                                   sep=",")
```

Compare signature probe selection
```{r}
dm_probes_by_ct.raw <- select_probes_t_separation_by_ct(betas=msorted_betas,
                                         cell_type_ind=msorted_pd$CellType,
                                         cell_types=CELL_TYPES, N=100)
raw_signature_probes <- unlist(dm_probes_by_ct.raw)

dm_probes_by_ct.combat <- select_probes_t_separation_by_ct(betas=msorted_betas.combat,
                                         cell_type_ind=msorted_pd.combat$CellType,
                                         cell_types=CELL_TYPES, N=100)
combat_signature_probes <- unlist(dm_probes_by_ct.combat)

# Draw the venn diagram.
raw_only <- setdiff(raw_signature_probes, combat_signature_probes)
combat_only <- setdiff(combat_signature_probes, raw_signature_probes)
shared <- intersect(raw_signature_probes, combat_signature_probes)
draw.pairwise.venn(area1=length(raw_signature_probes), 
                   area2=length(combat_signature_probes),
                   cross.area=length(shared), 
                   category = c("Raw", "Combat"))
# Where do the unstable probes belong
res <- sapply(dm_probes_by_ct.raw, function(ct_probes){sum(raw_only %in% ct_probes)})
barplot(res)

ct_idx <- msorted_pd$CellType %in% CELL_TYPES
plot_probes(msorted_betas[,ct_idx], raw_only, msorted_pd$CellType[ct_idx]) + ggtitle("Raw Only")
ct_idx <- msorted_pd.combat$CellType %in% CELL_TYPES
plot_probes(msorted_betas.combat[,ct_idx], combat_only, msorted_pd.combat$CellType[ct_idx]) + ggtitle("Combat Only")

ct_idx <- msorted_pd$CellType %in% CELL_TYPES
plot_probes(msorted_betas[,ct_idx], combat_only, msorted_pd$CellType[ct_idx]) + ggtitle("Combat Probes in Raw")

# Jaccard
print("Jaccard Index:")
length(shared) / length(union(raw_signature_probes, combat_signature_probes))

# Look at probe type distribution.
t1_probes <- getProbeInfo(msorted_rgset, type="I")$Name
t2_probes <- getProbeInfo(msorted_rgset, type="II")$Name
# Combat.
combat_signature_probes %>% intersect(t1_probes) %>% length()
combat_signature_probes %>% intersect(t2_probes) %>% length()
# Raw
raw_signature_probes %>% intersect(t1_probes) %>% length()
raw_signature_probes %>% intersect(t2_probes) %>% length()
```

Seems like the sorting by absolute value isn't exactly the best way to decide on cleanest probes.
Raw selection biases towards probes that have better mean separation, rather than "true" separation.

Weighted pairwise distinguishability?

Use combat probes on raw data.
```{r}
dm_probes_by_ct.combat <- select_probes_t_separation_by_ct(betas=msorted_betas.combat,
                                         cell_type_ind=msorted_pd.combat$CellType,
                                         cell_types=CELL_TYPES,
                                         N=100)
combat_signature_probes <- unlist(dm_probes_by_ct.combat)

# Use combat probes with the raw data.
cord_profile <- construct_profiles(betas=msorted_betas,
                                   probes=combat_signature_probes,
                                   cell_type_ind=msorted_pd$CellType, 
                                   cell_types=CELL_TYPES) 
est_cell_counts <- minfi:::projectCellType(validation_betas[rownames(cord_profile),],
                                           cord_profile,
                                           nonnegative = T,
                                           lessThanOne = T)
validation_data <- compare_estimation_to_truth(est_cell_counts, validation_counts)
plot_deconv_accuracy(validation_data, plot_scales="free") + ggtitle("Combat probes on Raw")

# Use combat probes with combat data.
cord_profile <- construct_profiles(betas=msorted_betas.combat,
                                   probes=combat_signature_probes,
                                   cell_type_ind=msorted_pd.combat$CellType, 
                                   cell_types=CELL_TYPES) 
est_cell_counts <- minfi:::projectCellType(validation_betas.combat[rownames(cord_profile),],
                                           cord_profile,
                                           nonnegative = T,
                                           lessThanOne = T)
validation_data <- compare_estimation_to_truth(est_cell_counts, validation_counts)
plot_deconv_accuracy(validation_data, plot_scales="free") + ggtitle("Combat probes on Combat")
```

Merge CD4T and CD8T
===================

Merge CD4T and CD8T into a cell type. Then redo selection.
Compare this to just predicting each one, and then merging predictions.
```{r}
ct48_idx <- sub("CD4T|CD8T", "CD48T", msorted_pd.combat$CellType)
CELL_TYPES48 <- c("Gran", "Mono", "Bcell", "nRBC", "NK", "CD48T")
dm_probes_by_ct.combat <- select_probes_t_separation_by_ct(betas=msorted_betas.combat,
                                         cell_type_ind=ct48_idx,
                                         cell_types= CELL_TYPES48,
                                         N=100)
combat_signature_probes <- unlist(dm_probes_by_ct.combat)
cord_profile <- construct_profiles(betas=msorted_betas.combat,
                                   probes=combat_signature_probes,
                                   cell_types=CELL_TYPES48, 
                                   cell_type_ind=ct48_idx)
est_cell_counts <- minfi:::projectCellType(validation_betas.combat[rownames(cord_profile),],
                                           cord_profile,
                                           nonnegative = T,
                                           lessThanOne = T)

validation_counts48 <- validation_counts %>% filter(CellType %in% c("CD4T", "CD8T")) %>%
                       group_by(SampleName) %>%
                       summarize(CellType="CD48T", True.Prop=sum(True.Prop))
validation_counts_merged <- rbind(validation_counts %>% filter(!(CellType %in% c("CD4T", "CD8T"))),
                                  validation_counts48)

# Plot against the counts.
validation_data <- compare_estimation_to_truth(est_cell_counts, validation_counts_merged)
plot_deconv_accuracy(validation_data, plot_scales = "free") +
  ggtitle("Merged 4T 8T selection")

# Do normal deconvolution, then merge 4T and 8T
dm_probes_by_ct.combat <- select_probes_t_separation_by_ct(betas=msorted_betas.combat,
                                         cell_type_ind=msorted_pd.combat$CellType,
                                         cell_types=CELL_TYPES,
                                         N=100)
combat_signature_probes <- unlist(dm_probes_by_ct.combat)
cord_profile <- construct_profiles(betas=msorted_betas.combat,
                                   probes=combat_signature_probes,
                                   cell_type_ind=msorted_pd.combat$CellType, 
                                   cell_types=CELL_TYPES) 
est_cell_counts <- minfi:::projectCellType(validation_betas.combat[rownames(cord_profile),],
                                           cord_profile,
                                           nonnegative = T,
                                           lessThanOne = T)
est_cell_counts48 <- est_cell_counts[,"CD4T"] + est_cell_counts[,"CD8T"]
est_cell_counts_merged <- cbind(est_cell_counts[,!(colnames(est_cell_counts) %in% c("CD4T", "CD8T"))],
                                CD48T=est_cell_counts48)

# Plot against the counts.
validation_data <- compare_estimation_to_truth(est_cell_counts_merged, validation_counts_merged)
plot_deconv_accuracy(validation_data, plot_scales = "free") + ggtitle("Merged After Selection")
```

