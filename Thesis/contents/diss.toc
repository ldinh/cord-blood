\contentsline {chapter}{Abstract}{ii}{chapter*.1}
\contentsline {chapter}{Lay Summary}{iii}{chapter*.2}
\contentsline {chapter}{Preface}{iv}{chapter*.3}
\contentsline {chapter}{Table of Contents}{v}{chapter*.4}
\contentsline {chapter}{List of Tables}{viii}{chapter*.5}
\contentsline {chapter}{List of Figures}{ix}{chapter*.6}
\contentsline {chapter}{Glossary}{xi}{chapter*.7}
\contentsline {chapter}{Acknowledgments}{xiii}{chapter*.8}
\contentsline {chapter}{Dedication}{xiv}{chapter*.9}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Epigenome Wide Association Studies}{2}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Epigenetics and DNAm}{2}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Measuring DNAm with Microarrays}{3}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Blood is a Complex Tissue}{4}{subsection.1.1.3}
\contentsline {section}{\numberline {1.2}Cell Type Heterogeneity in Association Studies}{5}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Confounding Due to Cell Type Heterogeneity}{5}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Computationally Correcting for Confounding}{6}{subsection.1.2.2}
\contentsline {section}{\numberline {1.3}Thesis Motivation}{6}{section.1.3}
\contentsline {section}{\numberline {1.4}Approach and Contribution}{8}{section.1.4}
\contentsline {section}{\numberline {1.5}Detailed Outline of Thesis}{8}{section.1.5}
\contentsline {chapter}{\numberline {2}Related Works}{10}{chapter.2}
\contentsline {section}{\numberline {2.1}The Linear Mixture Model for Complex Tissues}{10}{section.2.1}
\contentsline {section}{\numberline {2.2}Reference-Based Methods}{11}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Reference-Based Methods for Gene Expression}{12}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Reference-Based Methods for DNAm}{12}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}An Adult Blood DNAm Reference}{14}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Three Cord Blood DNAm References}{14}{subsection.2.2.4}
\contentsline {section}{\numberline {2.3}Reference Free Methods Applicable to DNAm}{14}{section.2.3}
\contentsline {chapter}{\numberline {3}Approach and Results}{18}{chapter.3}
\contentsline {section}{\numberline {3.1}Description of Datasets}{19}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Reference Cell Type Profiles}{19}{subsection.3.1.1}
\contentsline {subsubsection}{Adult Whole Blood Reference Cell Type Profiles}{19}{subsubsection*.10}
\contentsline {subsubsection}{Infant Cord Blood Reference Cell Type Profiles}{20}{subsubsection*.11}
\contentsline {subsection}{\numberline {3.1.2}Validation}{21}{subsection.3.1.2}
\contentsline {subsubsection}{Adult Whole Blood Validation}{21}{subsubsection*.13}
\contentsline {subsubsection}{Infant Cord Blood Validation}{21}{subsubsection*.14}
\contentsline {section}{\numberline {3.2}Evaluation Metrics}{21}{section.3.2}
\contentsline {section}{\numberline {3.3}Validation of Existing Estimation Methods}{22}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Approach}{23}{subsection.3.3.1}
\contentsline {subsubsection}{Measuring Estimation Performance Of Reference Datasets}{23}{subsubsection*.16}
\contentsline {subsubsection}{Comparing Reference Datasets}{24}{subsubsection*.17}
\contentsline {subsection}{\numberline {3.3.2}Accuracy of Adult Reference Dataset}{24}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Effects of Using a Cord Specific Reference}{29}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Comparison of Cord and Adult References}{30}{subsection.3.3.4}
\contentsline {subsection}{\numberline {3.3.5}Comparison of Three Cord Blood References}{34}{subsection.3.3.5}
\contentsline {section}{\numberline {3.4}Normalization}{39}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Approach}{39}{subsection.3.4.1}
\contentsline {subsubsection}{Baseline Normalization Procedure}{40}{subsubsection*.32}
\contentsline {subsubsection}{Steps to Optimize}{40}{subsubsection*.33}
\contentsline {subsection}{\numberline {3.4.2}Within Array Normalization}{42}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Between Array Normalization}{43}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Cluster Analysis of Full Normalization Pipeline}{47}{subsection.3.4.4}
\contentsline {section}{\numberline {3.5}Signature Selection}{47}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Approach}{47}{subsection.3.5.1}
\contentsline {subsubsection}{Constructing Cell Type Signature}{47}{subsubsection*.38}
\contentsline {subsubsection}{Investigating the Balancing of Highly and Lowly Methylated Probes}{49}{subsubsection*.39}
\contentsline {subsubsection}{Choosing A Signature Size}{49}{subsubsection*.40}
\contentsline {subsection}{\numberline {3.5.2}Balancing of Highly and Lowly Methylated Probes}{50}{subsection.3.5.2}
\contentsline {subsection}{\numberline {3.5.3}Finding an Optimal Signature Size}{51}{subsection.3.5.3}
\contentsline {subsection}{\numberline {3.5.4}Treating T-cells as Indistinguishable}{52}{subsection.3.5.4}
\contentsline {section}{\numberline {3.6}Evaluation}{53}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}Approach}{53}{subsection.3.6.1}
\contentsline {subsubsection}{Validating Estimation Accuracy Of A Cord Optimized Pipeline}{53}{subsubsection*.44}
\contentsline {subsubsection}{Changes To The Cell Type Signatures}{54}{subsubsection*.45}
\contentsline {subsubsection}{Comparison To Reference-Free Techniques}{54}{subsubsection*.46}
\contentsline {subsection}{\numberline {3.6.2}Validating Estimation Accuracy in Cord Blood}{55}{subsection.3.6.2}
\contentsline {subsection}{\numberline {3.6.3}Changes to the Cell Type Signatures}{57}{subsection.3.6.3}
\contentsline {subsection}{\numberline {3.6.4}Comparison to Reference-Free Techniques}{62}{subsection.3.6.4}
\contentsline {chapter}{\numberline {4}Conclusions}{69}{chapter.4}
\contentsline {chapter}{Bibliography}{72}{chapter*.62}
