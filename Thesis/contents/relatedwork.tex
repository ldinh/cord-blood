%% The following is a directive for TeXShop to indicate the main file
%%!TEX root = diss.tex

\chapter{Related Works}
\label{ch:RelatedWorks}

In this chapter, we discuss prior works on correction of \ac{CTH} in \ac{DNAm} studies. Section \ref{sec:linear-mixture-model} mathematically describes how measurements on a complex tissue can be modelled as a linear mixture.  We show how the two classes of \ac{CTH} correction techniques, reference-based and reference-free, arises naturally from this description. Section \ref{sec:reference-based-estimation} summarizes the most successful reference-based methods for \ac{CTP} estimation in blood. Section \ref{sec:reference-free-vars} summarizes existing reference-free techniques. 

\section{The Linear Mixture Model for Complex Tissues}
\label{sec:linear-mixture-model}

In 2001, \citet{venet-2001} presented the idea of computationally quantifying \ac{CTP} directly from the mixed microarray measurements. This was originally presented in the context of gene expression. Here, we summarize that framework in the context of \ac{DNAm}.

Assume that \ac{DNAm} measurements were made on a mixture of distinct cell types using a microarray. Then, for each \ac{DNAm} locus, the total measurement is the sum of signals from each cell type alone. We assume that the signal from each cell type is proportional to its relative abundance in the sample. This is called the linear mixing assumption, and it serves as a good model for the fluorescence intensities measured on microarrays \citep{houseman-2015-math}. 

Mathematically, we can represent the data generation process as follows:
\begin{itemize}
	\item L: The number of measured mixed samples.
	\item \textbf{M}: A matrix of measurements from DNAm microarrays. One column per mixed sample and one row per probe.
	\item \textbf{G}: A matrix of reference cell type profiles.  One column per cell type and one row per probe. 
	\item \textbf{C}: A matrix of concentrations. One row per cell type and one column per mixed sample. 
\end{itemize}

We assume that the mixed signal is generated only from the represented cell types in the columns of G: 
$$
\mathbf{M} = \mathbf{G}\mathbf{C}
$$ Since each column of C represents concentrations of cell types in a sample, each entry is between 0 and 1, and each column must sum to 1. We recognize that each column of \textbf{M} to be a linear mixture of the columns of \textbf{G} with weights defined by the corresponding column of \textbf{C}. 

In these microarray experiments, \textbf{M} is always observed. The problem of quantifying \ac{CTP} can be specialized into whether \textbf{G}, or a noisy representation of \textbf{G}, is available. Note that if \textbf{C} was observed, then we already have cell type proportions for all mixed samples which solves our original problem. When \textbf{G} is available, we are dealing with reference-based methods. If \textbf{G} is not available, then we must rely on reference-free methods. 

\section{Reference-Based Methods}
\label{sec:reference-based-estimation}

In the reference-based context, we have observed \textbf{G} and seek only to find an approximation of C, denoted $\hat{C}$. This is done on a sample-by-sample basis. The algorithm proceeds in two steps:
\begin{enumerate}
	\item \textit{Signature Selection}: Select an appropriate subset of rows, corresponding to probes, of \textbf{G}. The selected probes are called the cell type signature probes and the resulting submatrix of \textbf{G} is called the cell type signature. 
	\item \textit{Optimization}: For each column of \textbf{M}, denoted $\mathbf{M}_i$, solve the following for $1 \leq i \leq L$: 
		$$\min_{\hat{C}_i} \lVert  M_i - G \hat{C}_i \rVert $$ such that $\hat{C}_i$ constrained to the interval [0,1] and elements sum to less than or 
		equal to 1. Here the norm denotes some appropriate measure of distance between two vectors, for example Euclidian distance. 
\end{enumerate}

All reference-based estimation techniques rely on this formulation but differ in choice of signature selection heuristic, distance function, optimization procedure and enforcement of constraints on $\hat{C}_i$. 

\subsection{Reference-Based Methods for Gene Expression}

One of the early reference-based methods for cell type estimation from gene expression microarrays was presented by \citet{abbas-2009}.  Their method quantified the proportion of immune cell in adult whole blood samples. In the framework laid out above, signature selection was done by maximizing the condition number of the resulting \textbf{G} matrix, optimization was done with Euclidian distances, and the estimates had no enforced constraints. Their optimization is exactly the least squares method used in linear regression. As a post-hoc step, to get positive proportions, the process is run iteratively each time removing the most negative coefficient. Finally the results are normalized to sum to 1. 

The least-square method was subsequently extended to explicitly enforce the constraints as part of the optimization. One extension used non-negative least squares to enforce the lower bound of the constraint to get estimated proportions greater than 0 \citep{qiao-2012-pert}. Another extension used quadratic programming to ensure that the proportions were in the interval [0,1] and each sample's proportions summed to one \citep{gong-2011-qp}.  More recent methods move away from the least squares framework to more robust methods like Support Vector Regression \citep{newman-2015-svr}. 

\subsection{Reference-Based Methods for DNAm}

Correcting for \ac{CTH} in \ac{DNAm} studies, while conceptually similar, requires acknowledgement of some  \ac{DNAm}-specific realities. In 2012, \citet{houseman-2012-cp} proposed a method for estimating cell type proportion from \ac{DNAm} microarrays based. This method, once again, attempts to estimate the cell type proportions, $\hat{C}$, that best approximates \textbf{M} with $G\hat{C}$. Signature selection was done based on ordering F-statistics for CpGs. F-statistics were computed from independently fitting a linear mixed-effects model to each probe in the reference data to identify between cell type \acp{DMP}. The method uses Euclidian distances and quadratic programming to enforce constraints. 

Houseman's method was successfully applied to validation blood samples from a cohort of 94 healthy adult individuals \citep{koestler-2013-validation}. The study was done using Illumina's 27k technology, the predecessor to the 450k. Each sample was subject to \acp{CBC} and assayed on the \ac{DNAm} microarray. The \ac{DNAm} data, along with the reference data described in subsection \ref{adult-dna-ref}, was used to estimate \ac{CTP}. Since \acp{CBC} can only resolve cell types to the Lymphocyte, Monocyte and Granulocyte level, detailed estimates were aggregated appropriately. Estimates were shown to have low root-mean-squared-error (Lymphocytes: 5\%, Monocytes:6\%) and medium-high correlations (Lymphocytes:0.6, Monocytes:0.61). Thus, computational estimation of \ac{CTP} in \ac{DNAm} was demonstrated to be reasonably accurate in adult whole blood. 

In 2014, \citet{jaffe-2014-cth} extended Houseman's method to the Illumina 450k. This thesis builds upon Jaffe's method; so we carefully describe their approach here. Their method proceeds as follows:

\begin{enumerate}
	\item  \textit{Remove Bad Probes}: Remove probes on the 450k that are known to be problematic due to \acp{SNP} in the hybridizing sequence.
	\item \textit{Signature Selection}:  To form the matrix \textbf{G}, 100 probes were selected to distinguish each cell type. For each cell type, probes that were differentially methylated compared to all other cell types were identified using two-group t-tests. Probes with p-values $< 10^{-8}$ were ranked in order of effect size. The 100 most differentially methylated probes were selected, balanced between highly methylated (most positive difference in mean methylation) and lowly methylated (most negative difference in mean methylation).
	\item \textit{Optimization}: Estimation of $\mathbf{\hat{C}}$ is done by solving $$\min_{\hat{C}_i} \lVert  M_i - G \hat{C}_i \rVert_2 $$.\end{enumerate}

\subsection{An Adult Blood DNAm Reference}
\label{adult-dna-ref}

Currently, the most widely used adult whole blood reference dataset for \ac{DNAm} was published in 2012 \citep{reinius-2012-adultref}. Since then, it has been cited over 400 times. The dataset contains \ac{DNAm} profiles for 6 cell populations in adult whole blood: Gran, Mono, Bcell, NK, CD4T and CD8T. In their comparison, \citet{reinius-2012-adultref} showed that, between cell types, 85\% of human genes have at least one differentially methylated probe. They conclude that interpretation of whole blood methylation measurements should be done with great caution.

\subsection{Three Cord Blood DNAm References}

As \ac{DNAm} studies began focusing on cord blood, it became apparent that the existing \ac{CTP} estimation methods were not performing well in cord blood samples. In 2015, \citet{yousefi-2015} demonstrated that the Jaffe's method, paired with the adult reference-data, had very poor prediction accuracy for blood samples from new borns. The suggested culprit was a mismatch between the adult originated reference data and infant originated target samples.

To rectify this problem, three different cord blood reference datasets were generated \citep{gervin-2016-cordref, degoede-2015, bakulski-2016}. To date, only the dataset from The University of Oslo has been validated for \ac{CTP} estimation accuracy. In their study, \citet{gervin-2016-cordref} validated their reference dataset on an independent cohort of 195 individuals. Their results showed that using a cord-specific reference dataset improves correlation between estimates and cell counts, but low abundance cell types like Bcell and CD8T show only moderate correlations. 

In the next chapter, we report the \ac{CTP} estimation performance of these three references on a set of cord blood samples with matched cell counts.  We recommend one reference based on performance and coverage of cell types. Then, we show how estimation performance can be further improved by modifying the reference-based procedure itself.

\section{Reference Free Methods Applicable to DNAm}
\label{sec:reference-free-vars}

Reference-free methods allow for correction of \ac{CTH} when there is no available reference dataset. That is, these methods estimate both \textbf{G} and \textbf{C}, usually through matrix decomposition. Instead of directly estimation cell proportions, reference-free methods return surrogate variables that are functions of the cell proportions. These surrogate variables, similar to \ac{CTP} estimates, can be directly incorporated into downstream analyses to correct for \ac{CTH}. 

Reference-free methods must estimate both the profile, \textbf{G}, and the proportions, \textbf{C}, simultaneously from a set of \ac{DNAm} microarray samples. Estimating both \textbf{G} and \textbf{C} is hard because there are many more loci measured by these microarrays than there are number of samples. Formally this is a highly under-determined problem. Also, reference-free methods will capture other systemic sources of variation, like batch effects, in addition to variation from \ac{CTH}. Together, this makes reference-free results difficult to interpret directly.

In this section, we briefly outline the reference-free methods applicable to \ac{DNAm} studies. First, we cover methods first developed for gene expression studies, but applicable to \ac{DNAm} studies: SVA, ISVA, and RUV \citep{leek-2007-sva,  teschendorff-2011-isva, gagnon-2012-ruv}. Then we summarize methods developed in the context of \ac{DNAm} EWAS studies: LMMEWasher, RefFreeEWAS, and ReFACTOR \citep{houseman-2014-reffree, zou-2014-lmm, rahmani-2016-spca}.  ReFACTOr is given a detailed treatment because it serves as the reference-free comparison in this thesis. 

In 2007, \citet{leek-2007-sva} described \ac{SVA}, a SVD-based method for estimating unmodeled sources of variation. They called variation of expression due to these unmodeled sources \ac{EH}. Many of the following reference-free techniques build upon \ac{SVA}. The idea is to capture shared sources of variation between different observations of gene expression or \ac{DNAm}. Conceptually, \ac{SVA} proceeds through 3 steps:
\begin{enumerate}
	\item Remove the signal attributable to the main variables of interest to identify an orthogonal basis for \ac{EH}. 
	\item Find a subset of measurements associated with each basis of \ac{EH}. 
	\item Estimates the surrogate variables from the identified subsets using the original data. 	
\end{enumerate}
These surrogate variables are then incorporated into downstream regressions to control for the effects of confounding.

In 2011, \citet{teschendorff-2011-isva} extends upon \ac{SVA} from orthogonal surrogate variable to statistically independent surrogate variables. This is achieved by replacing \ac{SVD}, which enforced orthogonality, with \ac{ICA}. They show that \ac{ISVA} is more effective in cases when confounding is uncorrelated with the primary variables of interest in a non-linear fashion.

Another method for capturing unmodeled variation was presented by \citet{gagnon-2012-ruv} in 2012 and relies upon prior of knowledge genomic loci unaffected by the primary variable. This method, called \ac{RUV}, restricts the estimation of surrogate variables to a-priori unaffected negative control loci. By restricting the loci under consideration, \ac{RUV} mitigates the problem of overcorrecting for biological variation of interest. 

In 2014, \citet{houseman-2014-reffree} proposed a reference-free method called RefFreeEWAS. This method expanded upon \ac{SVA} by including the estimated covariates of the unadjusted model for differential methylation in the decomposition. They show algebraically how this expanded matrix better models the linear mixing assumption. Their method out performed \ac{SVA} when technical errors are small and variability is dominated by \ac{CTH}. 

Also in 2014, \citet{zou-2014-lmm} described a reference-free approach based on \acp{LMM} called EWASher. Originally, \acp{LMM} were used to control association study test statistic inflation due to genetic relatedness among inbred strains of model organisms like mice \citet{kang-2008}. By explicitly estimating the genetic relatedness of individuals, the model can better account for correlated measurements. To capture relatedness between samples, this approach computes pairwise methylome similarity between samples, and then includes this as the covariance component of the linear mixed model as a proxy for cell type composition The differential expression model is fit to observe inflation of test statistics. If there is inflation, the process is run iteratively with increasing number of \acl{PC} until test statistic inflation is controlled. 

In 2016, \citet{rahmani-2016-spca} described ReFACTOR, a reference-free method based on sparse \ac{PCA}. Intuitively, since cell type composition effects should be shared across many CpGs,  ReFACTOR tries to find a subset of probes that are well represented by a low-dimensional approximation of the observed samples. These resulting sparse factors should represent large scale effects like variation due to \ac{CTH}. The REFACTOR algorithm proceeds in 3 steps:

\begin{enumerate}
\item Find a  k-rank approximation of the sample matrix. Call this matrix $\tilde{O}$.
\item Look for the top d CpGs that are best approximated. If $O_i$ and $\tilde{O}_i$ represent the $i^{th}$ row of O and $\tilde{O}$ respectively, then find the d rows that have the smallest $distance(O_i, \tilde{O}_i)$.
\item Run PCA on the subset of d sites from (2), and return the scores for the top k principle components. 
\end{enumerate}

The resulting principle components are the sparse factors that should be a function of the \ac{CTP} and can be used in downstream correction of \ac{CTH}.

Since reference-free methods do not rely upon experimentally generated cell type profiles, they must make assumptions to bound the under-constrained solution space. These assumptions can differ quite substantially between algorithms, and usually an algorithm's performance depends greatly upon how well its' assumptions correspond to biological reality. For example, ReFACTOR and EWASher assume that the top components of variation are caused by cell type composition. In situations where this assumption is unfounded, these methods can overfit and remove true biological signal \citep{teschendorff-2017-review}. In contrast, SVA-based methods explicitly model out variation associated with the phenotype of interest before decomposing the residuals into surrogate variables. Thus, SVA-based methods no longer assumes that the largest components of variation are due to cell type. However, SVA-based methods rely upon having a well-specified model, which may not be available. In summary, reference-free methods are applicable to a wider range of tissues, but suffer from limitations like overfitting, unrealistic assumptions and model availability. 

In the next chapter, we compare reference-based estimates to reference-free surrogate variables in cord blood. Specifically, we examine the amount of variance captured by \ac{CTP} estimates versus surrogate variables. We show how ReFACTOR is able to accurately model variation attributable to abundant cell types like Gran, but tends to overcorrect when accounting for minor cell types. 