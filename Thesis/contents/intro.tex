%% The following is a directive for TeXShop to indicate the main file
%%!TEX root = diss.tex

\chapter{Introduction}
\label{ch:introduction}

The question of how molecular changes manifest as observable phenotypes has been asked since the discovery of DNA as the biological mechanism for inheritance. With the advent of genome sequencing, researchers began probing for genetic changes, like \acp{SNP}, that were enriched within populations with particular traits. This paradigm, called an association study, is increasingly popular as the cost of probing the genome continues to drop \citep{kruglyak-2008-gwas}.

DNA microarray technology is one particular development that facilitated the popularity of association studies. While early microarrays were limited to reading nucleotide base pairs, their capabilities have since been extended to other genomic features. Among other things, microarrays can now be used to quantify gene expression profiles or epigenetic marks genome-wide at the population scale.

While cost-effective and convenient, microarrays are not without drawbacks. Multiple studies have identified key challenges to ensuring high quality microarray-based analyses \citep{lappalainen-2017, laird-2010}.  These challenges originate from factors like measurement error and shortcomings in statistical methodology \citep{dedeurwaerder-2014-preprocess, benjamini-1995-fdr}. 

Researchers have overcome a number of these challenges, but one remaining issue is the confounding effects of \ac{CTH} in microarray association studies \citep{dedeurwaerder-2014-preprocess, jaffe-2014-cth}. The literature contains many algorithms to computationally correct for \ac{CTH}, but the problem is not satisfactorily resolved in all cases. One particularly troubling case arises in the study of human infant cord blood \ac{DNAm}. Cord blood is unusual because of its relatedness to adult blood, a case where \ac{CTH} in \ac{DNAm} studies has been successfully resolved \citep{houseman-2012-cp}.  In adult blood samples, effects due to \ac{CTH} are successfully mitigated through estimation of each sample's \ac{CTP}. But, it was observed that existing \ac{CTP} estimation techniques applied to cord blood suffer a severe degradation in performance \citep{yousefi-2015}.  Several previous attempts to close this adult-infant gap through better characterization of cord blood's constituent cell types did not fully succeed \citep{bakulski-2016, gervin-2016-cordref, degoede-2015}. In this thesis, we investigate the reasons behind estimation performance degradation and present a pipeline for more accurately estimating cell type proportions for cord blood \ac{DNAm} measurements. 
   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Epigenome Wide Association Studies}
\label{sec:ewas}

Association studies are used to link patterns in the epigenome to human phenotypes and diseases. These studies are called \ac{EWAS}. Often, association studies target \ac{DNAm}, an epigenetic mark, due to its' response to environmental factors, role in gene regulation and implications in development \citep{beck-2008}. These studies measure the \ac{DNAm}  level of up to millions of sites across the genome to find loci that exhibit a relationship to disease status or phenotype. 

\ac{EWAS} must employ large cohorts to detect small effects in a large number of measurements. This drives researchers towards accessible biological tissues like buccal or blood. These complex tissues, called complex tissues, consist of multiple cell types with distinct methylation profiles \citep{reinius-2012-adultref}. This mixing of distinct cell types makes inferring associations between methylation level and phenotypes a challenge. 

\subsection{Epigenetics and DNAm}

Scientific disciplines currently disagree on the definition of epigenetics \citep{deans-2015}. Some use the term to describe changes to gene expression, while others are explicitly referring to inheritance of expression patterns. In this thesis, we will follow the latter convention. Specifically, we use the term epigenetics to refer to mitotically heritable modifications to DNA that does not involve modifying the underlying base pair sequence \citep{teschendorff-2017-review, jones-2015-aging}. One such heritable epigenetic mark, and the focus of this thesis, is \ac{DNAm}.  

\acl{DNAm} is when a methyl group attaches to a DNA molecule. This process is crucial to development and involved in many biological processes like genomic imprinting, X-chromosome inactivation, and aging \citep{peters-2014, jones-2015-aging}. In mammals, like humans, DNA methylation occurs almost exclusively at the DNA base Cytosine immediately followed by Guanine, denoted a CpG site. Functionally, high levels of CpG methylation in promotor regions have been shown to negatively correlate with gene expression in multiple species \citep{horvath-2013}. \ac{DNAm} offers a mechanism by which somatic cells, all sharing the same DNA, can execute different genetic programs.

\ac{DNAm} plays a large role in cell differentiation and aging \citep{jaffe-2014-cth, jones-2015-aging}. Patterns of \ac{DNAm} are so closely tied to aging that it can be used to accurately predict biological age in humans \citep{horvath-2013}. In addition, \ac{DNAm} is highly sensitive to environmental and psychosocial factors such as stress, car exhaust or neonatal exposure to maternal cigarette smoke \citep{lam-2012, jiang-2014-exhaust}. Thus, many studies analyze \ac{DNAm} due to its key biological role, sensitivity to environmental factors, and heritability. 

\subsection{Measuring DNAm with Microarrays}
\label{dnam-microarray}

To study \ac{DNAm}'s role in gene regulation, aging and disease, we measure methylation levels across the genome. One platform for high throughput genome-wide measurement of \ac{DNAm} is Illumina's Infimium 450k. The 450k is a microarray based platform that interrogates the methylation status of 485,577 CpGs across the human genome \citep{price-2013-450anno}. The accuracy, sample requirements and relatively low cost make it the platform of choice for many \ac{EWAS} studies \citep{laird-2010}.

To measure methylation at CpG sites, the 450k uses probes designed to hybridize with DNA from a specific genomic locus \cite{dedeurwaerder-2011}. Hybridization is followed by an elongation step that causes differential fluorescence in methylated versus unmethylated sites. The amount of methylation is inferred from the strength of fluorescence. Due to the design of the 450k and inherent noise of physical experimentation, interpretation of \ac{DNAm} data requires care \citep{siegmund-2011, laird-2010}. 

One complication arising from the 450k's design is the distinction between Type 1 and Type 2 probes. To increase coverage of CpGs, the 450k includes two types of probes; here denoted as Type 1 and Type 2. Type 1 probes use two different physical beads to measure DNAm at a CpG; one each for methylated and unmethylated states. Type 2 probes use one physical bead and competitively binds to methylated and unmethylated DNA. Furthermore, the two probe types use different binding chemistries. This distinction is similar to the difference between two-colour and one-colour gene expression microarrays. As a result, Type 2 probes are less sensitive in the detection of extreme methylation values, and have greater variance between replicates \citep{dedeurwaerder-2011-pbc}.

Once measured, these raw DNAm fluorescence signals are transformed into either Beta values or M-values to facilitate analyses \citep{bibikova-2006, du-2010-mvalue}. Beta values can be intuitively interpreted as proportion of DNA molecules methylated at a particular site, say \textit{s}. Let F be the measured intensity of the fluorescence due to methylated molecules and let U be the measured intensity of fluorescence due to unmethylated molecules. Then 
$$
Beta_s = \frac{max(F,0)}{max(F,0) + max(U,0)}
$$ 
Another representation used in \ac{DNAm} analysis is the M-value. Beta values can be easily transformed to M-values as follows:
$$
M_s = log_2 \left( \frac{Beta_s}{1 - Beta_s} \right)
$$
This logit transformation results in better statistical properties such as meeting Gaussian assumptions, approximate homoscedasticy, and no longer being restricted to the interval between 0 and 1 \citep{du-2010-mvalue}. Such properties are desirable when performing statistical procedures like t-tests. In this thesis, both Beta values and M-values are used. 

\subsection{Blood is a Complex Tissue}
\label{sec:blood-biol}

As mentioned previously, one of the most common target tissues for \ac{EWAS} is whole blood.  While accessible, the heterogenous nature of blood complicates the interpretation of \ac{EWAS} analyses. Human blood is a mixture of cell populations with distinct methylation profiles \citep{reinius-2012-adultref}. In this thesis, we focus on the 7 major cell types found in human infant cord blood: granulocytes (Gran), CD14+monocytes (Mono), CD4+ T-cells (CD4T), CD8+ T-cells (CD8T), CD19+ B-cells (Bcell), CD56+ natural killer cells (NK) and nucleated red blood cells (nRBC). Previous findings show that these cell types can be differentially methylated at over 20\% of measured CpGs \citep{reinius-2012-adultref}. This differential methylation, paired with variability in \ac{CTP}, can make interpretations difficult, confound statistical associations and cause spurious discoveries. The next section discusses statistical confounding in detail.

Cord blood and adult blood, while similar, must be treated as distinct tissues. Adult blood contains only 6 of the 7 cord blood cell types mentioned; nucleated red blood cells are unique to cord blood \citep{degoede-2015}. In adults, red blood cells do not contain nuclei and therefore do not contribute to the methylation measurements. In cord blood, red blood cells are still in the process of extruding their nuclei and many still contain genetic material. Previous assays of nRBC \ac{DNAm} revealed an unusual methylation profile \citep{degoede-2015}. Their methylome did not exhibit a strong bimodal distribution like most cell types, and instead nRBCs had many intermediately methylated CpGs. In certain pregnancy complications, nRBCs can contribute up to 50\% of the genetic material measured \citep{aali-2007}.  Studies on cord blood must account for this unusual cell type. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Cell Type Heterogeneity in Association Studies}
\label{sec:statistical-confounding-ewas}

Researchers have long recognized that varying cell type composition across samples can dramatically affect the interpretation of association studies \citep{abbas-2009}. \ac{DNAm} studies are particularly susceptible to this type of confounding due to its tissue-specificity and highly variable nature \citep{lam-2012, houseman-2012-cp} . To compound the issue, \acp{CTP} in whole blood are not static over time. These proportions can change with environmental exposures, disease, and particularly age \citep{jaffe-2014-cth, jiang-2014-exhaust}. Thus DNAm studies using blood samples from different ages must be particularly vigilant in correcting for inter-individual differences in \ac{CTP}.  

\subsection{Confounding Due to Cell Type Heterogeneity}

Systemic differences in cell type proportion has long been recognized as a source of \ac{DNAm} variability \citep{lappalainen-2017, houseman-2012-cp}. Left unaccounted for, \ac{CTP} differences can lead to many false positive associations \citep{jaffe-2014-cth}. This problem is known as statistical confounding due to \ac{CTH}. For convenience, we sometimes refer to the problem as just \ac{CTH}. 

\ac{CTH} arises when comparing measurements from mixtures of cell types, when the underlying \acp{CTP} differ between samples. A detailed mathematical description of the problem can be found in Section \ref{sec:linear-mixture-model}. 

Experimental techniques for purifying cell populations can be used to resolve \ac{CTH}. For example, \ac{FACS} can be used to isolate pure cell populations before taking  \ac{DNAm} measurements. Directly comparing cells of the same type eradicates \ac{CTH}. However, experimental approaches suffers some limitations: affected cell types not known apriori, additional overhead, labour intensive, and cannot be performed post-hoc. These drawbacks make computational correction methods appealing. 

\subsection{Computationally Correcting for Confounding}

Techniques for correcting \ac{CTH} can be divided into two classes: reference-based \ac{CTP} estimation and reference-free surrogate variables \citep{mohammadi-2015}. Reference-based methods seek to accurately estimate the proportions of constituent cell types in a sample. These methods require reference cell type profiles - experimental measurements of \ac{DNAm} from purified constituent cell types. Reference-free methods do not require reference cell type profiles. However, this saving of experimental labour comes at the cost of interpretability. Reference-free methods result in surrogate, also called latent, variables that are a function of cell proportions rather than direct estimates.

Once computed, association studies can incorporate these reference-based estimates or reference-free surrogate variables in the same way \citep{lappalainen-2017}. Associations between \ac{DNAm} and phenotypes are typically inferred using linear models. Specifically, a linear model is fit  to each methylated position with the phenotype as an explanatory variable. Correcting for \ac{CTH} is done by expanding this set of explanatory variables to include either the \ac{CTP} estimates or the surrogate variables. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Thesis Motivation}
\label{Motivation}

	Computational methods that accurately estimate cell type proportions in \ac{DNAm} microarrays are convenient, highly interpretable and can be performed post-hoc on \ac{DNAm} EWAS. Methods correcting for blood samples are of particular interest due to their prevalence in population scale studies \citep{jaffe-2014-cth}. For adult whole blood, accurate methods for estimation of \ac{CTP} exists. However, attempts to develop an analogous method for human infant cord blood, a closely related tissue, have shown a persistent degradation in performance \citep{yousefi-2015, gervin-2016-cordref}.

Currently, the most accurate methods for estimating \ac{CTP} in adult whole blood rely upon the availability of reference cell type profiles.
In 2013, \citet{koestler-2013-validation} used these reference-based methods to estimate \acp{CTP} for 94 adult samples with matched blood counts; correlation for monocytes and lymphocytes were 0.6 and 0.61 respectively. In 2016, \citet{koestler-2016-idol} used an improved method to estimate \ac{CTP} for 6 adult samples with more detailed cell counts. They observed correlations over 0.99 for all cell types. However, it was observed that methods based on adult reference profiles failed to produce accurate estimates in cord blood \citep{yousefi-2015}. For cord blood, \citet{yousefi-2015} observed correlations for Monocytes and Lymphocytes of -0.01 and -0.03 respectively. 

Since methylation of cord blood cell types are known to be distinct from adult blood cell types, there have been several attempts at remedying this situation by developing cell type reference profiles specific to cord blood \citep{gervin-2016-cordref, bakulski-2016, degoede-2015}. However, even with cord blood specific reference profiles, estimation accuracy in cord blood was still low compared to their adult whole blood analog. In their 2016 study, \citet{gervin-2016-cordref} estimated cell counts for 195 cord blood samples using cord blood reference profiles. They observed \ac{CTP} correlations between 0.51 and 0.57 for low abundance cell types like Bcells and Monocytes. While better than adult references, cord blood estimation performance has room for improvement. Furthermore, this raises the concern that existing estimation methodologies cannot be extended to new tissues simply by characterizing that new tissue. 
 
An understanding of the culprits behind this loss of accuracy would benefit all cord blood based \ac{DNAm} EWAS. Furthermore, pinpointing culprits will assist in developing a better \ac{CTP} estimation method for cord blood DNAm. Finally, as the application of EWAS broadens to new tissue types, this understanding will help extend reference-based \ac{CTP} estimation methods to these new targets.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Approach and Contribution}
\label{Approach}

The objective of this thesis is to investigate how to improve the low estimation accuracy of cell type proportions in DNAm array measurements from human infant cord blood. Specifically, we aimed to improve upon a reference based method that accurately estimates cell type proportions in adult whole blood. Previous attempts at solving this problem by generating cord specific cell type reference profiles still has poor estimation accuracy in lowly abundant cell types. We assessed each step of the estimation procedure, identified problematic steps, resolved each issue specifically for cord blood samples, and validated the improved estimation method.

First, we confirmed how the adult estimation procedure is unsuitable for infant cord blood. To do so, we estimated cell type proportions using the same adult-calibrated procedure for both adult and infant samples. Estimates were compared to experimentally measured proportions to corroborate previously reported results. Indeed, adult-calibrated procedures are unsuitable for use on cord blood samples.

Next, we explored how the same procedure, but with cord blood specific cell type profiles, improves estimation accuracy. We used three different sets of cord blood specific cell type profiles and observed consistently low estimation accuracy, especially for low abundance lymphoid cells. Therefore, we concluded that the low accuracy is partially caused by the procedure itself.	

In order to improve estimation accuracy, we tailored the procedure to cord blood in a step-by-step fashion. A detailed outline of this thesis follows.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Detailed Outline of Thesis}
\label{detailed-outline-thesis}

The remainder of this thesis formalizes the problem of estimating cell type proportions in complex tissues [Chapter 2], surveys existing methods used to correct for cell type heterogeneity in association studies [Chapter 2], diagnoses the issues of applying existing methods to cord blood [Chapter 3], proposes resolutions and presents validation results for a new cord blood estimation pipeline [Chapter 3], and discusses directions for future work [Chapter 4]. 

A detailed chapter breakdown of this thesis is as follows:

\begin{itemize}

\item Chapter 2 surveys the existing literature on the problem of confounding due to cell type heterogeneity in association studies.  First, we present a linear model generating the mixed signal observed in complex tissues. We show how this model leads to the two classes of solutions for \ac{CTH}: reference-based and reference-free. We give reference-based estimation in \ac{DNAm} a more formal description and trace its origins to early applications in gene expression. Previous works on improving estimation of  \ac{CTP} in cord blood are also described. Finally, we touch upon reference-free techniques both specific to \ac{DNAm} and generally applicable to microarray studies. 

\item Chapter 3 presents the datasets, approach and experimental results used to improve the accuracy of \ac{CTP} estimation in cord blood. First, we describe the two validation sets of mixed tissues and four reference sets of cell type profiles. We also describe how performance of \ac{CTP} estimation was measured. Then we report results confirming previously reported results of low estimation accuracy using existing methods on our validation datasets. Next, we present a series of diagnostics used to determine the issues behind this low estimation accuracy. We show how resolving these issues lead to improved estimation accuracy. Finally, we detail a comparison between our reference-based estimates of cell type proportions to a PCA-based reference-free surrogate variables from the perspective of variance explained. 

\item Chapter 4 summarizes our results, identifies the limitations of reference-based estimation and discusses directions for future work. 

\end{itemize}

\endinput

Any text after an \endinput is ignored.
You could put scraps here or things in progress.
