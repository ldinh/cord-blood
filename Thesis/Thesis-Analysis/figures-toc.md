Figures:

1. Adult Reference - Adult reference doesn't work well on cord validation. [QN]
    a. Plot - Scatter of adult validation, Scatter of cord validation 
    b. Plot - barplot of correlations in adult validation and cord validation, barplot of  MAD in adult validation and cord validation
    d. Table - prediction accuracies in terms of both Corr. and MAD.
2. Cord Reference [UBC] on cord validation. [QN]
    a. Plot - Scatter on cord validation
    b. Plot - Effect Of Missing Cell Type nRBC histogram,  nRBC density as intermediate, dendrogram - misattribution due to missing CT
    b. Plot - Barplot against Koestler's benchmarks on validated adult data. Agg'ed to CBC level? (MAYBE?)
3. Comparison Adult to Cord References [QN with CB validation] - Adult and Cord CTs are very diff, despite being isolate with the same surface markers. 
    Result: Adult reference data isn't suitable for cord prediction.
    a. Plot - MDS of adult and cord types
    b. Plot - Heatmap of pairwise sig. diff probes on CT [Pval < .01, rowFtest on m-vals, BH correction on pvals]
    c. Plot - Barplot on # of variable sites.
    d. Table - # of pairwise DMRs pairwise
4. Alternative Cord Reference [UBC/JHU/Oslo] on Cord - The problem is general and not due to not sample size.
    => Summary June 2016/
    a. Plot - Barplot with Oslo/JHU/UBC
    b. Plot - Venn between Bak/JHU/UBC signature probes
    c. Plot - Scatter by Oslo/JHU/UBC
5. Normalization Diagnostics
    a. Plot - SVD Analysis of Background Association  [Noob, BMIQ, Quantile, SWAN]
    b. Plot - Cluster analysis Before/After 
    c. Plot - Pvalue inflation WB.
    d. Plot - Pvalue inflation between CTs.  [MAYBE]
6. Optimization Of Signature Size [Noob + BMIQ + ComBat]
    Goal: Find the optimal signature size for each cell type.
    Procedure is as follows:
        1. Split validation data into train/test set. (2-fold CV)
        2. On training set, build signature of size 1,3,5,...
        3. Predict on test.
        4. Goto 1
    => Optimize-Signature-Size
    => Email "Results For This Week" Mar. 28
    a. Plot - Scatter of Adult/Cord Up and Down plot
    b. Plot - Line plot of performance over sig size.
7. Corrected Deconvolution
    a. Plot - Scatter fixed DC.
    c. Plot - Barplot Adult, QN, ComBat.
    c. Plot - Staircase plot.
    d. Plot - Scree against CTs
    e. Plot - Merged scatter at CBC level
8. Comparison To Old Signature
    => Email "Update For This Week" June 9th
    => Compare-Sig-Before-After
    a. Plot - Venn diagram before/after
    b. Plot - Rank before/after
    c. Plot - Histogram on ranks dropped QN
    d. Plot - Histogram of ranks dropped CB
    e. Table - Jaccard Index.
9. Comparison To Reference Free [PC1 captures Gran/CD4T. correctly, many sites have Var. expl. by other CTs, sPCA misses these]
    => Residual Analysis.rmd
    a. Table - PC #, Variance explained by sPCA @ 500 sites, Correlation to CT prop.
    b. Table - Variance explained across genome by PCs + CT
    c. Plot - Scatter of sPC 1 on cord and correlation to Gran, and nothing else.
    d1. Plot - Scatter PC1 vs. Gran Var Expl. is well correlated across sites,
    d2. Plot -  Scatter PC1 vs. All CTs Var. Expl. not the same.,
    d3. Plot - PC1-6 != All Cts Var. Expl.
    e. Plot - Scatter Predicted CT versus Real CT is much better.

===== TODO: Extra Stuff === 

Idea - Identify sites that are driven by minor cell types. Run sPCA on those. Bet you can get that out too.

A. Merging Of References Doesn't help [Not a sample size issue]
    a. Plot - Add bar to barplot comparing 3 refs.
    d. Plot - Scatter by pooled Oslo + UBC

B. Merging of CD4 and CD8T
    c. Plot of signature probes CD4, CD8 and CD48 merged.
    d. Plot - Line plot with CD48 merge

