---
title: "cell-code-kobor"
author: "Louie Dinh"
date: "May 30, 2016"
output: html_document
---

```{r}
library(CellCODE)
library(RColorBrewer)
library(tidyr)
library(dplyr)

library(minfi)
source("../lib/deconvolution-utils.R")
```

Load the data and normalize.
```{r}
sheet <- read.450k.sheet("/Users/louie/Projects/cord-blood-deconvolution/Data-Raw/sorted")
sorted_cord_rgset <- read.450k.exp(targets=sheet)
sheet <- read.450k.sheet("/Users/louie/Projects/cord-blood-deconvolution/Data-Raw/validation")
validation_450k_rgset <- read.450k.exp(targets=sheet)

res <- normalize_together(referenceRG = sorted_cord_rgset, validationRG = validation_450k_rgset)
sorted_cord <- res$reference
validation_450k <- res$validation

# Data cleaning
sorted_cord$CellType <- to_canonical_cell_types(v = sorted_cord$Tissue,
                        cell_types = c("WB", "CBMC", "CD4T", "CD8T", "G", "Mo", "B", "NK", "RBC"),
                        canon_cell_types = c("WB", "CBMC", "CD4T", "CD8T", "Gran", "Mono", "Bcell", "NK", "nRBC"))
sorted_cord$Tissue <- NULL

# Map the array_ids to sample_ids
sample2array <- data.frame(SampleName=validation_450k$SampleName, ArrayName=sampleNames(validation_450k))
idx <- match(colnames(validation_450k), sample2array$ArrayName)
colnames(validation_450k) <- sample2array$SampleName[idx]
```

Pick markers based on our sorted set.
```{r}
# Construct the per-cell-type profile using means
sorted_pd <- pData(sorted_cord)
sorted_betas <- getBeta(sorted_cord)

cell_type_idxes <- splitit(sorted_pd$CellType)
cell_types <- names(cell_type_idxes)
ct_profile <- sapply(cell_types, function(ct){
  idx <- cell_type_idxes[[ct]]
  prof <- rowMeans(sorted_betas[,idx])
  prof  
})
names(ct_profile) <- cell_types
ct_profile <- ct_profile[,!colnames(ct_profile) %in% c("WB", "CBMC")]
cell_types <- colnames(ct_profile)

# Find tags.
sorted_tag=tagData(ct_profile, cutoff=.1, max=30)
names(sorted_tag) <- cell_types

validation_betas <- getBeta(validation_450k)
validation_pd <- pData(validation_450k)

SPVs=getAllSPVs(validation_betas, validation_pd$Sex, sorted_tag, method="raw", plot=F)
row.names(SPVs) <- colnames(validation_betas)
```

Calculate validaition rankings
```{r}
validation_counts <- read.csv("/Users/louie/Projects/cord-blood-deconvolution/Data/validation-counts.csv", header=T)
validation_counts <- gather(validation_counts, CellType, True.Prop, -SampleName)
validation_counts$True.Prop <- validation_counts$True.Prop / 100
validation_counts$CellType <- to_canonical_cell_types(validation_counts$CellType,
                        c("CD8T", "CD4T", "NK", "Bcell", "Monocytes", "Granulocytes", "nRBCs"),
                        c("CD8T", "CD4T","NK","Bcell","Mono", "Gran", "nRBC"))
```

Check against truth
```{r}
for(ct in cell_types) {
  ct_spv <- data.frame(row.names(SPVs), SPVs[,ct], ct)
  colnames(ct_spv) <- c("SampleName", "SPV", "CellType")
  validation_data <- inner_join(ct_spv, validation_counts, by=c("SampleName", "CellType"))
  c = cor(validation_data$True.Prop, validation_data$SPV)
  print(sprintf("Correlation for %s is %s", ct, c))
}
```