---
title: "adult-cellcode-spvs"
author: "Louie Dinh"
date: "May 31, 2016"
output: html_document
---

Based on cell-code-kobor
```{r}
library(CellCODE)
library(RColorBrewer)
library(tidyr)
library(dplyr)

library(minfi)
source("../lib/deconvolution-utils.R")
```

Load the data and normalize.
```{r}
# Load the adult data.
library(FlowSorted.Blood.450k)
reference_rgset <- FlowSorted.Blood.450k

pd <- read.table("koestler-validation-data-counts.csv", sep=",", header = T)
true_cell_counts <- gather(pd, CellType, True.Prop, -SampleName, -SampleType)
BASEDIR <- "/Users/louie/Projects/cord-blood-deconvolution/"
DATADIR <- paste(BASEDIR, "Data-External/koestler-validation-data", sep="/")
koestler_rgset <- read.450k.exp(base = DATADIR, recursive = TRUE)
# Attach some metadata to the RGSet
row.names(pd) <- pd$SampleName
adf <- as(pd, "AnnotatedDataFrame")
# This is surprisingly important. The dimLabels have to match the reference data!
dimLabels(adf) <- c("sampleNames", "sampleColumns")
phenoData(koestler_rgset) <- adf

res <- normalize_together(referenceRG = reference_rgset, validationRG = koestler_rgset)
sorted_adult <- res$reference
validation_adult <- res$validation

validation_pd <- pData(validation_adult)
validation_counts <- gather(data.frame(validation_pd) %>% select(CD4T, CD8T, Bcell, NK, Mono, Gran, SampleName), CellType, True.Prop, -SampleName)
```

Pick markers based on our sorted set.
```{r}
# Construct the per-cell-type profile using means
sorted_pd <- pData(sorted_adult)
sorted_betas <- getBeta(sorted_adult)

cell_type_idxes <- splitit(sorted_pd$CellType)
cell_types <- names(cell_type_idxes)
ct_profile <- sapply(cell_types, function(ct){
  idx <- cell_type_idxes[[ct]]
  prof <- rowMeans(sorted_betas[,idx])
  prof  
})
names(ct_profile) <- cell_types

# Select the profiles we are interested in.
ct_profile <- ct_profile[,colnames(ct_profile) %in% c("Gran", "Mono", "Bcell", "NK", "CD4T", "CD8T")]
cell_types <- colnames(ct_profile)

# Find tags.
sorted_tag=tagData(ct_profile, cutoff=.1, max=50)
names(sorted_tag) <- cell_types

validation_betas <- getBeta(validation_adult)
validation_pd <- pData(validation_adult)
```

Check the invivo data.
```{r}
invivo_samples_idx <- validation_pd$SampleType == "WB"
invivo_betas <- validation_betas[,invivo_samples_idx]
# Put in a random coding for the group variable because we arn't using it.
SPVs=getAllSPVs(invivo_betas, sample(c(1,2),size = ncol(invivo_betas), replace = T), sorted_tag, method="raw", plot=T)
row.names(SPVs) <- colnames(validation_betas[,invivo_samples_idx])

for(ct in cell_types) {
  ct_spv <- data.frame(row.names(SPVs), SPVs[,ct], ct)
  colnames(ct_spv) <- c("SampleName", "SPV", "CellType")
  validation_data <- inner_join(ct_spv, validation_counts, by=c("SampleName", "CellType"))
  c = cor(validation_data$True.Prop, validation_data$SPV)
  print(sprintf("Correlation for %s is %s", ct, c))
}
```

Check against invitro data.
```{r}
invitro_samples_idx <- validation_pd$SampleType == "WBC"
invitro_betas <- validation_betas[,invitro_samples_idx]
# Put in a random coding for the group variable because we arn't using it.
SPVs=getAllSPVs(invitro_betas, sample(c(1,2),size = ncol(invitro_betas), replace = T), sorted_tag, method="raw", plot=T)
row.names(SPVs) <- colnames(validation_betas[,invitro_samples_idx])

for(ct in cell_types) {
  ct_spv <- data.frame(row.names(SPVs), SPVs[,ct], ct)
  colnames(ct_spv) <- c("SampleName", "SPV", "CellType")
  validation_data <- inner_join(ct_spv, validation_counts, by=c("SampleName", "CellType"))
  c = cor(validation_data$True.Prop, validation_data$SPV)
  print(sprintf("Correlation for %s is %s", ct, c))
}
```