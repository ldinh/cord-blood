How the counts were derived:

CBCs
====
Under a microscope, we look at the different types of cells. Can identify Neut/Eo/Baso//Mono/Lymph/nRBC.

FACs
====
Isolate using different antibody markers.

Counts are done as fraction of total cells in sample.
Includes platelets, red blood cells, etc.
Sorting is done to get the actual methylation profiles on each subpopulation.

CD3 = Tcells
    CD4+/CD8- => CD4T
    CD8+/CD4- => CD8T
    Seems like there are many cells in Double Neg, Double Pos not counted.
CD3-, CD14+ = Monocytes
CD3-, CD235+ = nRBC
CD3-, CD19+ = Bcells [Sketchy flouresence here]
CD3-, CD14-, CD56+ = NK cells.

Gran measurement done through gradient for isolation.
