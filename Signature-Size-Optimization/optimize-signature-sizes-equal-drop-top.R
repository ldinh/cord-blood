probe_ratio_comparison <- function(validation_betas_training, cell_types, nstart, nend_seq) {
  
  dm_probes_by_ct <- select_probes_t_separation_by_ct(betas=msorted_betas,
                                                      cell_type_ind=msorted_pd$CellType,
                                                      cell_types=CELL_TYPES,
                                                      N=max(nend_seq))
  
  res <- data.frame()
  for(nend in nend_seq){
    # Build the signature
    probes <- unlist(lapply(dm_probes_by_ct, function(l){l[nstart:nend]}))
    cord_profile <- construct_profiles(betas=msorted_betas,
                                       probes=probes,
                                       cell_type_ind=msorted_pd$CellType, 
                                       cell_types=CELL_TYPES) 
    est_cell_counts <- minfi:::projectCellType(validation_betas_training[rownames(cord_profile),],
                                               cord_profile,
                                               nonnegative = T,
                                               lessThanOne = T)
    validation_data <- compare_estimation_to_truth(est_cell_counts, validation_counts)
    title <- paste("Nend:", nend, sep="")
    
    # Record the current result.
    curr_res <- validation_data %>% 
      mutate(adiff=abs(True.Prop-Est.Prop)) %>% 
      group_by(CellType) %>%
      summarize(Nstart=nstart,
                Nend=nend,
                Nprobes=(nend-nstart+1),
                MAD=mean(adiff) %>% round(2),
                Rsq=(cor(True.Prop, Est.Prop, method="pearson")^2) %>% round(2),
                Rho=cor(True.Prop, Est.Prop, method = "spearman") %>% round(2))
    res <- rbind(res, curr_res)
  }
  
  res
}

signature_size_opt <- function(validation_betas, training_ind, validation_counts,
                               cell_types, nstart=25, nend=75, nby=2, verbose=FALSE) {
  # Fold 1
  validation_betas_training <- validation_betas[,training_ind]
  validation_betas_test <- validation_betas[,!training_ind]
  
  nend_seq <- seq(nstart, nend, by=nby)
  perf_grid1 <- probe_ratio_comparison(validation_betas_training,
                                       cell_types=cell_types,
                                       nstart=nstart,
                                       nend_seq=nend_seq)
  
  if(verbose){
    title <- "Fold 1"
    p <- perf_grid1 %>% ggplot(aes(Nprobes, Rho, col=CellType)) +
      geom_line() + ggtitle(title)
    print(p)
  }
  
  # Find "optimal" signature size and do prediction on test set.
  dm_probes_by_ct <- select_probes_t_separation_by_ct(betas=msorted_betas,
                                                      cell_type_ind=msorted_pd$CellType,
                                                      cell_types=CELL_TYPES,
                                                      N=max(nend_seq))
  opt1 <- perf_grid1 %>% group_by(CellType) %>% arrange(-Rho) %>%
    filter(row_number() == 1) %>% arrange(CellType) %>% ungroup()
  
  if(verbose){
    print("Fold 1:")
    print(opt1)
  }
  
  probes <- NULL
  for(ct in cell_types) {
    nend <- opt1 %>% filter(CellType == ct) %>% select(Nend) %>% unlist()
    probes <- c(dm_probes_by_ct[[ct]][nstart:nend], probes)
  }
  # Construct profile and predict for our test data.
  cord_profile <- construct_profiles(betas=msorted_betas,
                                     probes=probes,
                                     cell_type_ind=msorted_pd$CellType, 
                                     cell_types=CELL_TYPES) 
  est_cell_counts <- minfi:::projectCellType(validation_betas_test[rownames(cord_profile),],
                                             cord_profile,
                                             nonnegative = T,
                                             lessThanOne = T)
  validation_data_test1 <- compare_estimation_to_truth(est_cell_counts, validation_counts)
  validation_data_test1$Fold=1
  
  # Fold 2 - Notice that now training/test have been flipped!
  validation_betas_training <- validation_betas[,!training_ind]
  validation_betas_test <- validation_betas[,training_ind]
  perf_grid2 <- probe_ratio_comparison(validation_betas_training, cell_types=cell_types,
                                       nstart=nstart,
                                       nend_seq=nend_seq)
  
  if(verbose){
    title <- "Fold 2"
    p <- perf_grid2 %>% ggplot(aes(Nprobes, Rho, col=CellType)) +
      geom_line() + ggtitle(title)
    print(p)
  }
  
  # Find "optimal" signature size and do prediction on test set.
  dm_probes_by_ct <- select_probes_t_separation_by_ct(betas=msorted_betas,
                                                      cell_type_ind=msorted_pd$CellType,
                                                      cell_types=CELL_TYPES,
                                                      N=max(nend_seq))
  opt2 <- perf_grid2 %>% group_by(CellType) %>% arrange(-Rho) %>%
    filter(row_number() == 1) %>% arrange(CellType) %>% ungroup()
  
  if(verbose){
    print("Fold 2:")
    print(opt2)
  }
  
  probes <- NULL
  for(ct in cell_types) {
    nend <- opt2 %>% filter(CellType == ct) %>% select(Nend) %>% unlist()
    probes <- c(dm_probes_by_ct[[ct]][nstart:nend], probes)
  }
  # Construct profile and predict for our test data.
  cord_profile <- construct_profiles(betas=msorted_betas,
                                     probes=probes,
                                     cell_type_ind=msorted_pd$CellType, 
                                     cell_types=CELL_TYPES) 
  est_cell_counts <- minfi:::projectCellType(validation_betas_test[rownames(cord_profile),],
                                             cord_profile,
                                             nonnegative = T,
                                             lessThanOne = T)
  validation_data_test2 <- compare_estimation_to_truth(est_cell_counts, validation_counts)
  validation_data_test2$Fold=2
  
  # Pack the objects for return.
  perf_grid1$Fold <- 1
  perf_grid2$Fold <- 2
  list(predictions=rbind(validation_data_test1, validation_data_test2),
       sig_size1=cbind(opt1, Fold=1),
       sig_size2=cbind(opt2, Fold=2),
       perf_grid=rbind(perf_grid1, perf_grid2))
}