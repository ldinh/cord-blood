---
title: "sig-size-opt-cv-small-steps"
author: "Louie Dinh"
date: '2017-02-14'
output: html_document
---

Load the data.
```{r}
source("../lib/deconvolution-utils.R")
library(ggplot2)

CELL_TYPES <- c("CD8T","CD4T", "NK", "Bcell", "Mono", "Gran", "nRBC")

# Load in the counts
validation_counts <- read.csv("/Users/louie/Projects/cord-blood-deconvolution/Data/validation-counts.csv", header=T)
validation_counts <- gather(validation_counts, CellType, True.Prop, -SampleName)
validation_counts$True.Prop <- validation_counts$True.Prop / 100
validation_counts$CellType <- to_canonical_cell_types(validation_counts$CellType,
                        c("CD8T", "CD4T", "NK", "Bcell", "Monocytes", "Granulocytes", "nRBCs"),
                        c("CD8T","CD4T","NK","Bcell","Mono", "Gran", "nRBC"))


# Load the cleaned up data.
msorted_betas <- read.table("../Data/sorted_betas_filter_noob_combat.csv", sep=",") %>% as.matrix()
msorted_pd <- read.table("../Data/sorted_pd_filter_noob_combat.csv", sep=",")
validation_betas <- read.table("../Data/validation_betas_filter_noob_combat.csv",
                                      sep=",") %>% as.matrix()
validation_pd <- read.table("../Data/validation_pd_filter_noob_combat.csv",
                                   sep=",")
```

Inner Loop (2-fold CV):
1. Split data into 2, training/test.
2. Do selection + prediction on training => gives a grid of ct + nprobes + Rho.
3. Predict test.
4. Swap training/test and goto 1.

Outer Loop:
1. Assign split.
2. Run inner loop for prediction accuracy on each validation sample.
3. Do this 20x, to get 20 different predictions and 20 different "optimized signatures"

TODO: Add print statements during execution.  

```{r}
source("optimize-signature-size.R")
# Generates grid of prediction accuracy on training set.
```

```{r, warning=FALSE}
# Try running it once.
set.seed(1)
N = ncol(validation_betas)
N_BASE <- 10
N_MAX <- 150
N_BY <- 2
VERBOSE <- TRUE

predictions <- data.frame()
meta <- data.frame()
validation_sample_dist <- NULL
for(i in 1:20){
  training_ind <- rep(FALSE, times=N)
  training_idx <- sample(1:N, size=N/2)
  training_ind[training_idx] = TRUE
  
  print("Training Index:")
  print(training_idx)
  res <- signature_size_opt(validation_betas=validation_betas,
                            training_ind=training_ind,
                            validation_counts=validation_counts,
                            cell_types=CELL_TYPES,
                            nbase=N_BASE,
                            nmax=N_MAX,
                            nby=N_BY,
                            verbose=VERBOSE)
  
  preds <- res$predictions
  preds$Run = i
  print(res$sig_size1)
  print(res$sig_size2)
  
  # Store predictions, and signature sizes.
  predictions <- rbind(predictions, preds)
  temp1 <- res$sig_size1; temp1$Run = i
  temp2 <- res$sig_size2;  temp2$Run = i
  meta <- rbind(meta, temp1, temp2)
  validation_sample_dist <- rbind(validation_sample_dist, training_idx)
  
}

preds_summary <- predictions %>% group_by(CellType, Run) %>%
  summarize(Rho=cor(Est.Prop, True.Prop, method="spearman"))
# Write results down.
write.csv(predictions, "raw-predictions-10.csv", row.names = F)
write.csv(preds_summary, "predictions-summary-10.csv", row.names=F)
write.csv(meta, "run-meta-10.csv", row.names=F)
write.csv(validation_sample_dist, "validation-sample-fold-dist-10.csv")

# Summarize Predictions
ggplot(preds_summary, aes(factor(Run), Rho, fill=factor(Run))) +
  geom_bar(stat="identity") + 
  facet_wrap( ~ CellType) + 
  ggtitle("Accuracy by CellType") + vertical_x_labels
ggplot(preds_summary, aes(CellType, Rho, fill=factor(Run))) +
  geom_bar(stat="identity") + 
  facet_wrap( ~ Run) + vertical_x_labels + ggtitle("Accuracy by Run") +
  vertical_x_labels

# Summarize Signature sizes.
ggplot(meta, aes(CellType, Nprobes, group=Fold, fill=factor(Fold))) +
  geom_bar(stat="identity", position="dodge") + facet_wrap(~ Run) +
  ggtitle("Signature size by Run") + vertical_x_labels
ggplot(meta, aes(factor(Run), Nprobes, group=Fold, fill=factor(Fold))) +
  geom_bar(stat="identity", position="dodge") + facet_wrap(~ CellType) +
  ggtitle("Signature Size by Run") + vertical_x_labels
```

```{r, warning=F}
# Try running it once.
set.seed(1)
N = ncol(validation_betas)
N_BASE <- 50
N_MAX <- 150
N_BY <- 2
VERBOSE <- TRUE

predictions <- data.frame()
meta <- data.frame()
validation_sample_dist <- NULL
for(i in 1:20){
  training_ind <- rep(FALSE, times=N)
  training_idx <- sample(1:N, size=N/2)
  training_ind[training_idx] = TRUE
  
  print("Training Index:")
  print(training_idx)
  res <- signature_size_opt(validation_betas=validation_betas,
                            training_ind=training_ind,
                            validation_counts=validation_counts,
                            cell_types=CELL_TYPES,
                            nbase=N_BASE,
                            nmax=N_MAX,
                            nby=N_BY,
                            verbose=VERBOSE)
  
  preds <- res$predictions
  preds$Run = i
  print(res$sig_size1)
  print(res$sig_size2)
  
  # Store predictions, and signature sizes.
  predictions <- rbind(predictions, preds)
  temp1 <- res$sig_size1; temp1$Run = i
  temp2 <- res$sig_size2;  temp2$Run = i
  meta <- rbind(meta, temp1, temp2)
  validation_sample_dist <- rbind(validation_sample_dist, training_idx)
  
}

preds_summary <- predictions %>% group_by(CellType, Run) %>%
  summarize(Rho=cor(Est.Prop, True.Prop, method="spearman"))
# Write results down.
write.csv(predictions, "raw-predictions-50.csv", row.names = F)
write.csv(preds_summary, "predictions-summary-50.csv", row.names=F)
write.csv(meta, "run-meta-50.csv", row.names=F)
write.csv(validation_sample_dist, "validation-sample-fold-dist-50.csv")

# Summarize Predictions
ggplot(preds_summary, aes(factor(Run), Rho, fill=factor(Run))) +
  geom_bar(stat="identity") + 
  facet_wrap( ~ CellType) + 
  ggtitle("Accuracy by CellType") + vertical_x_labels
ggplot(preds_summary, aes(CellType, Rho, fill=factor(Run))) +
  geom_bar(stat="identity") + 
  facet_wrap( ~ Run) + vertical_x_labels + ggtitle("Accuracy by Run") +
  vertical_x_labels

# Summarize Signature sizes.
ggplot(meta, aes(CellType, Nprobes, group=Fold, fill=factor(Fold))) +
  geom_bar(stat="identity", position="dodge") + facet_wrap(~ Run) +
  ggtitle("Signature size by Run") + vertical_x_labels
ggplot(meta, aes(factor(Run), Nprobes, group=Fold, fill=factor(Fold))) +
  geom_bar(stat="identity", position="dodge") + facet_wrap(~ CellType) +
  ggtitle("Signature Size by Run") + vertical_x_labels
```

```{r, warning=FALSE}
# Try running it once.
set.seed(1)
N = ncol(validation_betas)
N_BASE <- 100
N_MAX <- 150
N_BY <- 2
VERBOSE <- TRUE

predictions <- data.frame()
meta <- data.frame()
validation_sample_dist <- NULL
for(i in 1:20){
  training_ind <- rep(FALSE, times=N)
  training_idx <- sample(1:N, size=N/2)
  training_ind[training_idx] = TRUE
  
  print("Training Index:")
  print(training_idx)
  res <- signature_size_opt(validation_betas=validation_betas,
                            training_ind=training_ind,
                            validation_counts=validation_counts,
                            cell_types=CELL_TYPES,
                            nbase=N_BASE,
                            nmax=N_MAX,
                            nby=N_BY,
                            verbose=VERBOSE)
  
  preds <- res$predictions
  preds$Run = i
  print(res$sig_size1)
  print(res$sig_size2)
  
  # Store predictions, and signature sizes.
  predictions <- rbind(predictions, preds)
  temp1 <- res$sig_size1; temp1$Run = i
  temp2 <- res$sig_size2;  temp2$Run = i
  meta <- rbind(meta, temp1, temp2)
  validation_sample_dist <- rbind(validation_sample_dist, training_idx)
  
}

preds_summary <- predictions %>% group_by(CellType, Run) %>%
  summarize(Rho=cor(Est.Prop, True.Prop, method="spearman"))
# Write results down.
write.csv(predictions, "raw-predictions-100.csv", row.names = F)
write.csv(preds_summary, "predictions-summary-100.csv", row.names=F)
write.csv(meta, "run-meta-100.csv", row.names=F)
write.csv(validation_sample_dist, "validation-sample-fold-dist-100.csv")

# Summarize Predictions
ggplot(preds_summary, aes(factor(Run), Rho, fill=factor(Run))) +
  geom_bar(stat="identity") + 
  facet_wrap( ~ CellType) + 
  ggtitle("Accuracy by CellType") + vertical_x_labels
ggplot(preds_summary, aes(CellType, Rho, fill=factor(Run))) +
  geom_bar(stat="identity") + 
  facet_wrap( ~ Run) + vertical_x_labels + ggtitle("Accuracy by Run") +
  vertical_x_labels

# Summarize Signature sizes.
ggplot(meta, aes(CellType, Nprobes, group=Fold, fill=factor(Fold))) +
  geom_bar(stat="identity", position="dodge") + facet_wrap(~ Run) +
  ggtitle("Signature size by Run") + vertical_x_labels
ggplot(meta, aes(factor(Run), Nprobes, group=Fold, fill=factor(Fold))) +
  geom_bar(stat="identity", position="dodge") + facet_wrap(~ CellType) +
  ggtitle("Signature Size by Run") + vertical_x_labels
```