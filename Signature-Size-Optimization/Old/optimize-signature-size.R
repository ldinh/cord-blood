probe_ratio_comparison <- function(validation_betas_training, N_BASE, N_MAX, N_BY) {
  
  dm_probes_by_ct <- select_probes_t_separation_by_ct(betas=msorted_betas,
                                                      cell_type_ind=msorted_pd$CellType,
                                                      cell_types=CELL_TYPES,
                                                      N=N_MAX)
  
  res <- data.frame()
  for(ct in CELL_TYPES) {
    for(nprobes in seq(0, N_MAX, by=N_BY)) {
      # Build the signature, selecting N_BASE for normal CTs.
      # Extra for our extra cell type
      cts_other <- CELL_TYPES[CELL_TYPES != ct]
      ct_probes <- dm_probes_by_ct[[ct]][1:nprobes]
      other_probes <- unlist(lapply(dm_probes_by_ct[cts_other], function(l){l[1:N_BASE]}))
      
      cord_profile <- construct_profiles(betas=msorted_betas,
                                         probes=c(ct_probes, other_probes),
                                         cell_type_ind=msorted_pd$CellType, 
                                         cell_types=CELL_TYPES) 
      est_cell_counts <- minfi:::projectCellType(validation_betas_training[rownames(cord_profile),],
                                                 cord_profile,
                                                 nonnegative = T,
                                                 lessThanOne = T)
      validation_data <- compare_estimation_to_truth(est_cell_counts, validation_counts)
      title <- paste("CT:", ct, " Nprobes:", nprobes, " Nbase:", N_BASE, sep="")
      
      # Record the current result.
      curr_res <- validation_data %>% filter(CellType == ct) %>%
        mutate(adiff=abs(True.Prop-Est.Prop)) %>% group_by(CellType) %>%
        summarize(Nprobes=nprobes, Nbase=N_BASE,
                  ProbeRatio=nprobes/N_BASE,
                  MAD=mean(adiff) %>% round(2),
                  Rsq=(cor(True.Prop, Est.Prop, method="pearson")^2) %>% round(2),
                  Rho=cor(True.Prop, Est.Prop, method = "spearman") %>% round(2))
      res <- rbind(res, curr_res)
    }
  }
  
  res
}

signature_size_opt <- function(validation_betas, training_ind, validation_counts,
                               N_BASE=10, N_MAX=30, N_BY=5, verbose=FALSE){
  # Fold 1
  validation_betas_training <- validation_betas[,training_ind]
  validation_betas_test <- validation_betas[,!training_ind]
  perf_grid <- probe_ratio_comparison(validation_betas_training, N_BASE=N_BASE, N_MAX=N_MAX, N_BY=N_BY) 
  
  if(verbose){
    title <- paste("Base: ", N_BASE, " Fold: 1", sep="")
    p <- perf_grid %>% ggplot(aes(ProbeRatio, Rho, col=CellType)) +
        geom_line() + geom_vline(xintercept=1) + ggtitle(title)
    print(p)
  }
  
  # Find "optimal" signature size and do prediction on test set.
  dm_probes_by_ct <- select_probes_t_separation_by_ct(betas=msorted_betas,
                                                      cell_type_ind=msorted_pd$CellType,
                                                      cell_types=CELL_TYPES,
                                                      N=N_MAX)
  opt1 <- perf_grid %>% group_by(CellType) %>% arrange(-Rho) %>%
    filter(row_number() == 1) %>% arrange(CellType) %>% ungroup()
  
  if(verbose){
    print("Fold 1:")
    print(opt)
  }
  
  probes <- NULL
  for(ct in CELL_TYPES) {
    nprobes <- opt1 %>% filter(CellType == ct) %>% select(Nprobes) %>% unlist()
    probes <- c(dm_probes_by_ct[[ct]][0:nprobes], probes)
  }
  # Construct profile and predict for our test data.
  cord_profile <- construct_profiles(betas=msorted_betas,
                                     probes=probes,
                                     cell_type_ind=msorted_pd$CellType, 
                                     cell_types=CELL_TYPES) 
  est_cell_counts <- minfi:::projectCellType(validation_betas_test[rownames(cord_profile),],
                                             cord_profile,
                                             nonnegative = T,
                                             lessThanOne = T)
  validation_data_test1 <- compare_estimation_to_truth(est_cell_counts, validation_counts)
  validation_data_test1$Fold=1
  
  # Fold 2 - Notice that now training/test have been flipped!
  validation_betas_training <- validation_betas[,!training_ind]
  validation_betas_test <- validation_betas[,training_ind]
  perf_grid <- probe_ratio_comparison(validation_betas_training, N_BASE=N_BASE, N_MAX=N_MAX, N_BY=N_BY) 
  
  if(verbose){
    title <- paste("Base: ", N_BASE, " Fold: 2", sep="")
    p <- perf_grid %>% ggplot(aes(ProbeRatio, Rho, col=CellType)) +
      geom_line() + geom_vline(xintercept=1) + ggtitle(title)
    print(p)
  }
  
  # Find "optimal" signature size and do prediction on test set.
  dm_probes_by_ct <- select_probes_t_separation_by_ct(betas=msorted_betas,
                                                      cell_type_ind=msorted_pd$CellType,
                                                      cell_types=CELL_TYPES,
                                                      N=N_MAX)
  opt2 <- perf_grid %>% group_by(CellType) %>% arrange(-Rho) %>%
    filter(row_number() == 1) %>% arrange(CellType) %>% ungroup()
  
  if(verbose){
    print("Fold 2:")
    print(opt2)
  }
  
  probes <- NULL
  for(ct in CELL_TYPES) {
    nprobes <- opt2 %>% filter(CellType == ct) %>% select(Nprobes) %>% unlist()
    probes <- c(dm_probes_by_ct[[ct]][0:nprobes], probes)
  }
  # Construct profile and predict for our test data.
  cord_profile <- construct_profiles(betas=msorted_betas,
                                     probes=probes,
                                     cell_type_ind=msorted_pd$CellType, 
                                     cell_types=CELL_TYPES) 
  est_cell_counts <- minfi:::projectCellType(validation_betas_test[rownames(cord_profile),],
                                             cord_profile,
                                             nonnegative = T,
                                             lessThanOne = T)
  validation_data_test2 <- compare_estimation_to_truth(est_cell_counts, validation_counts)
  validation_data_test2$Fold=2
  
  list(predictions=rbind(validation_data_test1, validation_data_test2),
       sig_size1=cbind(opt1, Fold=1),
       sig_size2=cbind(opt2, Fold=2))
}
