November 12th 2016
==================
- Deconvolution of sorted samples (WB), compared to the blood counts is very accurate [Sorted Samples Deconvolution]
- We can do quantile normalization, one validation sample at a time, with the sorted samples. Improves R^2 slightly. [Blood Counts Only]
- We can do a standardization normalization, which removes the bias [.09 MAD improvement] but kills our signal [lose .1 R^2].
- nRBC count could be untrustworthy as a representation of methylation added by RBCs. 

TODO Tomorrow: 
    - Try recenter the validation data, rather than adjust the stdev?
    - Try estimating with aggregation of only one group at a time. That way we will get a more accurate residuals beta.
