Can we add error bounds to estimate? 
How can we add a prior knowledge on expected CT props? [Prior on x]
How do we incorporate the fact that CT profiles are *not* identical to CTs in mixed sample? [Prior on A]
How does this compare to reference-free approaches?
SVA, Sparse PCA, Supervised PCA
Does selection procedure introduce bias  into signature? [like multiple testing]
Can we try an iterative approach to deconvolution?
How did normalization change the probe signatures?

